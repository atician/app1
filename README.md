# Tic Tac Toe

Tic Tac Toe game based off of [React's tutorial](https://reactjs.org/tutorial/tutorial.html) with one or two enhancements.

I encourage you to try out multi player mode. If there is no one else to play with, you can always open up the app on another tab and play against yourself.


Check it out [here](https://limitless-ravine-16827.herokuapp.com/).

## Getting Started

Make sure you have [Node.js](https://nodejs.org/) installed.

Clone this repo

`git clone https://atician@bitbucket.org/atician/app1.git`

`cd app1`

Install dependencies

```
npm install
cd client && npm install
cd ../server && npm install
```

Start App in Dev mode

```
cd ..
npm run dev
```

Finally, head over to http://localhost:3000/

## Tests

Client tests

`npm run client-test`

Server tests

`npm run server-test`

Tests can also be run from the `client` or `server` directories with

`npm test`

## Built With

* [React](https://github.com/facebook/create-react-app) - Front end
* [Express](https://expressjs.com/) - Back end
* [Redux](https://redux.js.org/) - State management
* [Socket.io](https://socket.io/) - Real time communication
* [Nodemon](https://nodemon.io/) - Auto server restart
* [Jest](https://jestjs.io/) - JS Unit testing
* [Enzyme](https://github.com/airbnb/enzyme) - React Component testing
* And many others

## Thanks

* [React/Redux Links - markerikson](https://github.com/markerikson/react-redux-links) for the library of knowledge on github, and stack overflow.
* [React/Redux - gaearon](https://github.com/gaearon) for the extensive docs on React and Redux and resources on github, stack overflow, and twitter.
* [MDN](https://developer.mozilla.org/en-US/) for the complete API reference and resources.
* [CSS-Tricks](https://css-tricks.com/) for helping me make my site pretty.
* [IcoMoon](https://icomoon.io/#home) for the free high quality icons.
* [Never Stop Building](https://www.neverstopbuilding.com/blog/minimax) for wrapping my head around Tic-Tac-Toe's AI.
* And many others


## License

[MIT](LICENSE.md)
