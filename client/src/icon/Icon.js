import React from 'react';
import styled from 'styled-components';
import { string } from 'prop-types';

import * as icons from '../constants/icons';

const Icon = ({ name }) => {
  const icon = icons[name];
  return (
    <SVG preserveAspectRatio="xMidYMin slice" viewBox="-1.3 -1.3 18 18">
      {icon && <path
        fill={icon.color}
        stroke={icon.color}
        d={icon.path}/>}
    </SVG>
  );
};
Icon.propTypes = {
  name: string
};
const SVG = styled.svg`
  pointer-events: none;
  width: 60%;
  padding-bottom: 60%;
  height: 1px;
  overflow: visible;
`;

export default Icon;
