import reducer from './currPlayer';
import * as types from '../constants/actionTypes';

describe('currPlayer reducer', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toBeNull();
  });
  test('should return previous state for unhandled type', () => {
    expect(reducer('player_1', {
      type: types.POST_PLAYER_FAILURE
    })).toBe('player_1');
  });
  test('should handle POST_START_SUCCESS', () => {
    expect(reducer(undefined, {
      type: types.POST_START_SUCCESS,
      currPlayer: 'player_1'
    })).toBe('player_1');

    expect(reducer('player_1', {
      type: types.POST_START_SUCCESS,
      currPlayer: 'player_2'
    })).toBe('player_2');
  });
  test('should handle POST_RESET_SUCCESS', () => {
    expect(reducer(undefined, {
      type: types.POST_RESET_SUCCESS,
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CIRCLE',
            score: 0
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      }
    })).toBe('player_1');

    expect(reducer('player_1', {
      type: types.POST_RESET_SUCCESS,
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CIRCLE',
            score: 0
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 1
          }
        },
        ids: ['player_2', 'player_1']
      }
    })).toBe('player_2');
  });
});
