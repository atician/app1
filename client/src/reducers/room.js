import { POST_PLAYER_SUCCESS } from '../constants/actionTypes';

const room = (state = null, action) => {
  switch (action.type) {
    case POST_PLAYER_SUCCESS:
      return action.room;
    default:
      return state;
  }
}
export default room;
