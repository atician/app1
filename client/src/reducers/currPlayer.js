import {
  POST_START_SUCCESS,
  EVENT_START,
  POST_MOVE_SUCCESS,
  EVENT_MOVE,
  POST_RESET_SUCCESS,
  EVENT_RESET
} from '../constants/actionTypes';

const currPlayer = (state = null, action) => {
  switch (action.type) {
    case POST_START_SUCCESS:
    case EVENT_START:
    case POST_MOVE_SUCCESS:
    case EVENT_MOVE:
      return action.currPlayer;
    case POST_RESET_SUCCESS:
    case EVENT_RESET:
      // set current player to first player on leader board
      return action.players.ids[0];
    default:
      return state;
  }
};
export default currPlayer;
