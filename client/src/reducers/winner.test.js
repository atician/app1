import reducer from './winner';
import * as types from '../constants/actionTypes';

describe('winner reducer', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toBeNull();
  });
  test('should return previous state for unhandled type', () => {
    expect(reducer('player_1', {
      type: types.POST_PLAYER_FAILURE
    })).toBe('player_1');
  });
  test('should handle POST_RESET_SUCCESS', () => {
    expect(reducer('player_1', {
      type: types.POST_RESET_SUCCESS
    })).toBeNull();

    expect(reducer(null, {
      type: types.POST_RESET_SUCCESS
    })).toBeNull();
  });
  test('should handle POST_MOVE_SUCCESS', () => {
    expect(reducer(null, {
      type: types.POST_MOVE_SUCCESS,
      winner: null
    })).toBeNull();

    expect(reducer(null, {
      type: types.POST_MOVE_SUCCESS,
      winner: 'player_2'
    })).toBe('player_2');
  });
});
