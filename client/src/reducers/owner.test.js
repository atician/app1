import reducer from './owner';
import * as types from '../constants/actionTypes';

describe('owner reducer', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toBeNull();
  });
  test('should return previous state for unhandled type', () => {
    expect(reducer('player_1', {
      type: types.POST_PLAYER_FAILURE
    })).toBe('player_1');
  });
  test('should handle ADD_PLAYER', () => {
    expect(reducer(undefined, {
      type: types.ADD_PLAYER,
      player: {
        id: 'player_1',
        name: 'Joe',
        avatar: null,
        score: 0
      }
    })).toBe('player_1');
  });
  test('should handle CONNECT_SOCKET_SUCCESS', () => {
    expect(reducer(undefined, {
      type: types.CONNECT_SOCKET_SUCCESS,
      socket: {
        id: 'player_1'
      }
    })).toBe('player_1');
  });
});
