import reducer from './mode';
import * as types from '../constants/actionTypes';
import * as gameConstants from '../constants/gameConstants';

describe('mode reducer', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toBeNull();
  });
  test('should return previous state for unhandled type', () => {
    expect(reducer(gameConstants.MULTI_PLAYER, {
      type: types.POST_PLAYER_FAILURE,
      mode: gameConstants.SINGLE_PLAYER
    })).toBe(gameConstants.MULTI_PLAYER);
  });
  test('should handle SET_MODE', () => {
    expect(reducer(undefined, {
      type: types.SET_MODE,
      mode: gameConstants.SINGLE_PLAYER
    })).toBe(gameConstants.SINGLE_PLAYER);

    expect(reducer(gameConstants.SINGLE_PLAYER, {
      type: types.SET_MODE,
      mode: gameConstants.MULTI_PLAYER
    })).toBe(gameConstants.MULTI_PLAYER);
  });
});
