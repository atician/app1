import { ADD_PLAYER, CONNECT_SOCKET_SUCCESS } from '../constants/actionTypes';

const owner = (state = null, action) => {
  switch (action.type) {
    case ADD_PLAYER:
      return action.player.id;
    case CONNECT_SOCKET_SUCCESS:
      // owner's id is the socket io's id for multi player mode
      return action.socket.id;
    default:
      return state;
  }
};
export default owner;
