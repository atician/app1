import {
  CONNECT_SOCKET_FAILURE,
  GET_PLAYER_FAILURE,
  POST_PLAYER_FAILURE,
  PATCH_AVATAR_FAILURE,
  POST_START_FAILURE,
  POST_MOVE_FAILURE,
  POST_RESET_FAILURE,
  POST_MESSAGE_FAILURE
} from '../constants/actionTypes';

const error = (state = null, action) => {
  switch (action.type) {
    case CONNECT_SOCKET_FAILURE:
    case GET_PLAYER_FAILURE:
    case POST_PLAYER_FAILURE:
    case PATCH_AVATAR_FAILURE:
    case POST_START_FAILURE:
    case POST_MOVE_FAILURE:
    case POST_RESET_FAILURE:
    case POST_MESSAGE_FAILURE:
      return action.error;
    default:
      return null;
  }
};
export default error;
