import { POST_MESSAGE_SUCCESS, EVENT_MESSAGE } from '../constants/actionTypes';

const initialState = {
  byId: {},
  ids: []
};
const messages = (state = initialState, action ) => {
  switch (action.type) {
    case POST_MESSAGE_SUCCESS:
    case EVENT_MESSAGE:
      return {
        byId: {
          ...state.byId,
          [action.message.id]: {...action.message}
        },
        ids: [...state.ids, action.message.id]
      };
    default:
      return state;
  }
};
export default messages;
