import {
  POST_START_SUCCESS,
  EVENT_START,
  POST_RESET_SUCCESS,
  EVENT_RESET,
  POST_MOVE_SUCCESS,
  EVENT_MOVE
} from '../constants/actionTypes';

// available moves on the board are set to null
// positions on the board with a player's move are set to the player's id
const moves = (state = null, action) => {
  switch (action.type) {
    case POST_START_SUCCESS:
    case EVENT_START:
      return Array(action.maxMoves).fill(null);
    case POST_RESET_SUCCESS:
    case EVENT_RESET:
      return Array(state.length).fill(null);
    case POST_MOVE_SUCCESS:
    case EVENT_MOVE:
      return [
        ...state.slice(0, action.position),
        action.player,
        ...state.slice(action.position + 1)
      ];
    default:
      return state;
  }
};
export default moves;
