import {
  POST_RESET_SUCCESS,
  EVENT_RESET,
  POST_MOVE_SUCCESS,
  EVENT_MOVE
} from '../constants/actionTypes';

const winner = (state = null, action) => {
  switch (action.type) {
    case POST_RESET_SUCCESS:
    case EVENT_RESET:
      return null;
    case POST_MOVE_SUCCESS:
    case EVENT_MOVE:
      return action.winner;
    default:
      return state;
  }
};
export default winner;
