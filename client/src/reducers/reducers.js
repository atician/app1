import { combineReducers } from 'redux';
import * as types from '../constants/actionTypes';
import owner from './owner';
import mode from './mode';
import room from './room';
import currPlayer from './currPlayer';
import winner from './winner';
import moves from './moves';
import players from './players';
import messages from './messages';
import isLoading from './isLoading';
import error from './error';

const socket = (state = null, action) => {
  switch (action.type) {
    case types.CONNECT_SOCKET_SUCCESS:
      return action.socket;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  socket,
  owner,
  mode,
  room,
  players,
  currPlayer,
  moves,
  winner,
  messages,
  isLoading,
  error
});

export default rootReducer;
