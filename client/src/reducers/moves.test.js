import reducer from './moves';
import * as types from '../constants/actionTypes';
import * as gameConstants from '../constants/gameConstants';

describe('moves reducer', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toBeNull();
  });
  test('should return previous state for unhandled type', () => {
    expect(reducer(Array(gameConstants.MAX_MOVES).fill(null), {
      type: types.POST_PLAYER_FAILURE
    })).toEqual(Array(gameConstants.MAX_MOVES).fill(null));
  });
  test('should handle POST_START_SUCCESS', () => {
    expect(reducer(undefined, {
      type: types.POST_START_SUCCESS,
      maxMoves: gameConstants.MAX_MOVES
    })).toEqual(Array(gameConstants.MAX_MOVES).fill(null));
  });
  test('should handle POST_RESET_SUCCESS', () => {
    expect(reducer([
      null, 'player_1', null,
      'player_2', 'player_1', null,
      'player_2', 'player_1', null
    ], {
      type: types.POST_RESET_SUCCESS
    })).toEqual(Array(gameConstants.MAX_MOVES).fill(null));
  });
  test('should handle POST_MOVE_SUCCESS', () => {
    expect(reducer(Array(gameConstants.MAX_MOVES).fill(null), {
      type: types.POST_MOVE_SUCCESS,
      position: 0,
      player: 'player_1'
    })).toEqual([
      'player_1', null, null,
      null, null, null,
      null, null, null
    ]);

    expect(reducer([
      null, 'player_1', null,
      'player_2', 'player_1', null,
      'player_2', null, null
    ], {
      type: types.POST_MOVE_SUCCESS,
      position: 7,
      player: 'player_1'
    })).toEqual([
      null, 'player_1', null,
      'player_2', 'player_1', null,
      'player_2', 'player_1', null
    ]);

    expect(reducer([
      null, 'player_1', null,
      'player_2', 'player_1', null,
      'player_2', null, null
    ], {
      type: types.POST_MOVE_SUCCESS,
      position: 8,
      player: 'player_2'
    })).toEqual([
      null, 'player_1', null,
      'player_2', 'player_1', null,
      'player_2', null, 'player_2'
    ]);
  });
});
