import reducer from './room';
import * as types from '../constants/actionTypes';

describe('room reducer', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toBeNull();
  });
  test('should return previous state for unhandled type', () => {
    expect(reducer('room_1', {
      type: types.POST_PLAYER_FAILURE,
      room: 'room_2'
    })).toBe('room_1');
  });
  test('should handle POST_PLAYER_SUCCESS', () => {
    expect(reducer(undefined, {
      type: types.POST_PLAYER_SUCCESS,
      room: 'room_1'
    })).toBe('room_1');

    expect(reducer('room_1', {
      type: types.POST_PLAYER_SUCCESS,
      room: 'room_2'
    })).toBe('room_2');
  });
});
