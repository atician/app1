import { SET_MODE } from '../constants/actionTypes';

const mode = (state = null, action) => {
  switch (action.type) {
    case SET_MODE:
      return action.mode;
    default:
      return state;
  }
};
export default mode;
