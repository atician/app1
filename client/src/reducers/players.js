import {
  ADD_PLAYER,
  ADD_PC_PLAYER,
  PATCH_AVATAR_SUCCESS,
  EVENT_AVATAR,
  GET_PLAYER_SUCCESS,
  EVENT_PLAYER,
  POST_RESET_SUCCESS,
  EVENT_RESET
} from '../constants/actionTypes';

const initialState = {
  byId: {},
  ids: []
};
const players = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PLAYER:
    case ADD_PC_PLAYER:
      return {
        byId: {
          ...state.byId,
          [action.player.id]: {
            ...action.player
          }
        },
        ids: [...state.ids, action.player.id]
      };
    case PATCH_AVATAR_SUCCESS:
    case EVENT_AVATAR:
      // update player's avatar
      return {
        byId: {
          ...state.byId,
          [action.player]: {
            ...state.byId[action.player],
            'avatar': action.avatar
          }
        },
        ids: [...state.ids]
      };
    case GET_PLAYER_SUCCESS:
    case EVENT_PLAYER:
    case POST_RESET_SUCCESS:
    case EVENT_RESET:
      return action.players;
    default:
      return state;
  }
};
export default players;
