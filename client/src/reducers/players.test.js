import reducer from './players';
import * as types from '../constants/actionTypes';
import * as gameConstants from '../constants/gameConstants';

describe('players reducer', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual({byId: {}, ids: []});
  });
  test('should return previous state for unhandled type', () => {
    expect(reducer({byId: {}, ids: []}, {
      type: types.PATCH_AVATAR_FAILURE,
      player: {
        id: 'player_1',
        name: 'Joe',
        avatar: null,
        score: 0
      }
    })).toEqual({byId: {}, ids: []});
  });
  test('should handle ADD_PLAYER', () => {
    expect(reducer(undefined, {
      type: types.ADD_PLAYER,
      player: {
        id: 'player_1',
        name: 'Joe',
        avatar: null,
        score: 0
      }
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1']
    });
  });
  test('should handle ADD_PC_PLAYER', () => {
    expect(reducer(undefined, {
      type: types.ADD_PC_PLAYER,
      player: {
        id: 'PC_PLAYER',
        name: 'PC',
        avatar: 'CROSS',
        score: 0
      }
    })).toEqual({
      byId: {
        PC_PLAYER: {
          id: 'PC_PLAYER',
          name: 'PC',
          avatar: 'CROSS',
          score: 0
        }
      },
      ids: ['PC_PLAYER']
    });
  });
  test('should handle ADD_PLAYER and ADD_PC_PLAYER', () => {
    expect(reducer(undefined, {
      type: types.ADD_PLAYER,
      player: {
        id: 'player_1',
        name: 'Joe',
        avatar: null,
        score: 0
      }
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1']
    });

    expect(reducer({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1']
    }, {
      type: types.ADD_PC_PLAYER,
      player: {
        id: 'PC_PLAYER',
        name: 'PC',
        avatar: 'CROSS',
        score: 0
      }
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        },
        PC_PLAYER: {
          id: 'PC_PLAYER',
          name: 'PC',
          avatar: 'CROSS',
          score: 0
        }
      },
      ids: ['player_1', 'PC_PLAYER']
    });
  });
  test('should handle PATCH_AVATAR_SUCCESS', () => {
    expect(reducer({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1']
    }, {
      type: types.PATCH_AVATAR_SUCCESS,
      player: 'player_1',
      avatar: 'CIRCLE'
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 0
        }
      },
      ids: ['player_1']
    });
  });
  test('should handle EVENT_AVATAR', () => {
    expect(reducer({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        },
        PC_PLAYER: {
          id: 'PC_PLAYER',
          name: 'PC',
          avatar: 'CROSS',
          score: 0
        }
      },
      ids: ['player_1', 'PC_PLAYER']
    }, {
      type: types.EVENT_AVATAR,
      player: 'player_1',
      avatar: 'CIRCLE'
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 0
        },
        PC_PLAYER: {
          id: 'PC_PLAYER',
          name: 'PC',
          avatar: 'CROSS',
          score: 0
        }
      },
      ids: ['player_1', 'PC_PLAYER']
    });
  });
  test('should handle GET_PLAYER_SUCCESS', () => {
    expect(reducer({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 0
        }
      },
      ids: ['player_1']
    }, {
      type: types.GET_PLAYER_SUCCESS,
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CIRCLE',
            score: 0
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      }
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 0
        },
        player_2: {
          id: 'player_2',
          name: 'Jane',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1', 'player_2']
    });
  });
  test('should handle EVENT_PLAYER', () => {
    expect(reducer({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1']
    }, {
      type: types.EVENT_PLAYER,
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: null,
            score: 0
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CROSS',
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      }
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        },
        player_2: {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CROSS',
          score: 0
        }
      },
      ids: ['player_1', 'player_2']
    });
  });
  test('should handle POST_RESET_SUCCESS', () => {
    expect(reducer({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 0
        },
        player_2: {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CROSS',
          score: 0
        }
      },
      ids: ['player_1', 'player_2']
    }, {
      type: types.POST_RESET_SUCCESS,
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CIRCLE',
            score: 1
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CROSS',
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      }
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 1
        },
        player_2: {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CROSS',
          score: 0
        }
      },
      ids: ['player_1', 'player_2']
    });
  });
  test('should handle EVENT_RESET', () => {
    expect(reducer({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 1
        },
        player_2: {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CROSS',
          score: 0
        }
      },
      ids: ['player_1', 'player_2']
    }, {
      type: types.EVENT_RESET,
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CIRCLE',
            score: 1
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CROSS',
            score: 1
          }
        },
        ids: ['player_1', 'player_2']
      }
    })).toEqual({
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 1
        },
        player_2: {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CROSS',
          score: 1
        }
      },
      ids: ['player_1', 'player_2']
    });
  });
});
