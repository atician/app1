import reducer from './messages';
import * as types from '../constants/actionTypes';

describe('messages reducer', () => {
  test('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual({byId: {}, ids: []});
  });
  test('should return previous state for unhandled type', () => {
    expect(reducer({byId: {}, ids: []}, {
      type: types.POST_PLAYER_FAILURE
    })).toEqual({byId: {}, ids: []});
  });
  test('should handle POST_MESSAGE_SUCCESS', () => {
    expect(reducer({byId: {}, ids: []}, {
      type: types.POST_MESSAGE_SUCCESS,
      message: {
        id: 'message_1',
        player: 'player_1',
        text: 'North'
      }
    })).toEqual({
      byId: {
        message_1: {
          id: 'message_1',
          player: 'player_1',
          text: 'North'
        }
      },
      ids: ['message_1']
    });

    expect(reducer({
      byId: {
        message_1: {
          id: 'message_1',
          player: 'player_1',
          text: 'North'
        }
      },
      ids: ['message_1']
    }, {
      type: types.POST_MESSAGE_SUCCESS,
      message: {
        id: 'message_2',
        player: 'player_2',
        text: 'South'
      }
    })).toEqual({
      byId: {
        message_1: {
          id: 'message_1',
          player: 'player_1',
          text: 'North'
        },
        message_2: {
          id: 'message_2',
          player: 'player_2',
          text: 'South'
        }
      },
      ids: ['message_1', 'message_2']
    });
  });
});
