import React, { Component } from 'react';
import { connect } from 'react-redux';

import AvatarChooser from './AvatarChooser';
import { setAvatar, startGame } from '../actions/actions';
import {
  handleAvatar,
  handleStart,
  addSocketListener,
  removeSocketListener
} from '../actions/actionHelpers';
import { EVENT_AVATAR, EVENT_START } from '../constants/actionTypes';

export class AvatarChooserContainer extends Component {
  componentDidMount() {
    this.props.dispatch(addSocketListener(EVENT_AVATAR, setAvatar));
    this.props.dispatch(addSocketListener(EVENT_START, startGame));
  }
  componentWillUnmount() {
    this.props.dispatch(removeSocketListener(EVENT_AVATAR));
    this.props.dispatch(removeSocketListener(EVENT_START));
  }
  render() {
    return (
      <AvatarChooser {...this.props}/>
    );
  }
}
AvatarChooserContainer.propTypes = {
  ...AvatarChooser.propTypes
};
export const mapStateToProps = ({ owner, players }) => ({
  isLeader: players.ids[0] === owner,
  selectedAvatars: players.ids
      .map(id => players.byId[id].avatar)
      .filter(avatar => avatar)
});
export const mapDispatchToProps = dispatch => ({
  dispatch,
  handleAvatar: (e) => dispatch(handleAvatar(e.target.name)),
  handleStart: () => dispatch(handleStart())
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AvatarChooserContainer);
