import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {
  AvatarChooserContainer,
  mapStateToProps,
  mapDispatchToProps
} from './AvatarChooserContainer';
import AvatarChooser from './AvatarChooser';

jest.mock('../actions/actionHelpers');
jest.mock('../actions/actions');
import * as actionHelpers from '../actions/actionHelpers';
import * as actions from '../actions/actions';
import { EVENT_AVATAR, EVENT_START } from '../constants/actionTypes';

Enzyme.configure({adapter: new Adapter()});

let props, wrapper;

describe('AvatarChooserContainer', () => {
  beforeEach(() => {
    props = {
      isLeader: false,
      selectedAvatars: [],
      handleAvatar: jest.fn(),
      handleStart: jest.fn(),
      dispatch: jest.fn()
    };
    wrapper = shallow(<AvatarChooserContainer {...props}/>);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('should render component', () => {
    expect(wrapper.find(AvatarChooser).exists()).toBe(true);
    // componentDidMount
    expect(props.dispatch).toHaveBeenCalledTimes(2);
    expect(actionHelpers.addSocketListener.mock.calls).toEqual([
      [EVENT_AVATAR, actions.setAvatar],
      [EVENT_START, actions.startGame]
    ]);
    props.dispatch.mockClear();
    // componentWillUnmount
    wrapper.unmount();
    expect(props.dispatch).toHaveBeenCalledTimes(2);
    expect(actionHelpers.removeSocketListener.mock.calls).toEqual([
      [EVENT_AVATAR],
      [EVENT_START]
    ]);
  });
  describe('mapStateToProps', () => {
    test(`isLeader should be true if owner is the
        first player on the leader board`, () => {
      // owner is the first and only player
      expect(mapStateToProps({
        owner: 'player_1',
        players: {
          byId: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: null,
              score: 0
            }
          },
          ids: ['player_1']
        }
      })).toEqual({
        isLeader: true,
        selectedAvatars: []
      });
      // owner is the first player
      expect(mapStateToProps({
        owner: 'player_1',
        players: {
          byId: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: null,
              score: 0
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: null,
              score: 0
            }
          },
          ids: ['player_1', 'player_2']
        }
      })).toEqual({
        isLeader: true,
        selectedAvatars: []
      });
      // owner is not the first player
      expect(mapStateToProps({
        owner: 'player_1',
        players: {
          byId: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: null,
              score: 0
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: null,
              score: 0
            }
          },
          ids: ['player_2', 'player_1']
        }
      })).toEqual({
        isLeader: false,
        selectedAvatars: []
      });
    });
    test('selectedAvatars should be the avatars set in players', () => {
      // no avatars selected
      expect(mapStateToProps({
        owner: 'player_1',
        players: {
          byId: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: null,
              score: 0
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: null,
              score: 0
            }
          },
          ids: ['player_2', 'player_1']
        }
      })).toEqual({
        isLeader: false,
        selectedAvatars: []
      });
      // CROSS selected
      expect(mapStateToProps({
        owner: 'player_1',
        players: {
          byId: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: 'CROSS',
              score: 0
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: null,
              score: 0
            }
          },
          ids: ['player_2', 'player_1']
        }
      })).toEqual({
        isLeader: false,
        selectedAvatars: ['CROSS']
      });
      // CROSS and CIRCLE selected
      const newProps = mapStateToProps({
        owner: 'player_1',
        players: {
          byId: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: 'CROSS',
              score: 0
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: 'CIRCLE',
              score: 0
            }
          },
          ids: ['player_2', 'player_1']
        }
      });
      expect(newProps.isLeader).toBe(false);
      // order of avatars doesn't matter
      expect(newProps.selectedAvatars.length).toBe(2);
      expect(newProps.selectedAvatars).toContain('CIRCLE');
      expect(newProps.selectedAvatars).toContain('CROSS');
    });
  });
  describe('mapDispatchToProps', () => {
    test('handleAvatar should be called with the avatar name', () => {
      mapDispatchToProps(props.dispatch).handleAvatar({target: {name: 'CROSS'}});
      expect(actionHelpers.handleAvatar).toHaveBeenCalledTimes(1);
      expect(actionHelpers.handleAvatar).toHaveBeenCalledWith('CROSS');
    });
    test('handleStart action helper should have been called', () => {
      mapDispatchToProps(props.dispatch).handleStart();
      expect(actionHelpers.handleStart).toHaveBeenCalledTimes(1);
    });
  });
});
