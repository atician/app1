import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import AvatarChooser from './AvatarChooser';
import Button from '../utils/Button';
import Icon from '../icon/Icon';
import * as icons from '../constants/icons';

Enzyme.configure({adapter: new Adapter()});

let props, wrapper;

describe('AvatarChooser', () => {
  beforeEach(() => {
    props = {
      isLeader: false,
      selectedAvatars: [],
      handleAvatar: jest.fn(),
      handleStart: jest.fn()
    };
    wrapper = shallow(<AvatarChooser {...props}/>);
  });
  test('should render component', () => {
    const buttons = wrapper.find(Button);
    expect(
      buttons.find({name: 'CIRCLE'}).find('Icon[name="CIRCLE"]').exists()
    ).toBe(true);
    expect(
      buttons.find({name: 'CROSS'}).find('Icon[name="CROSS"]').exists()
    ).toBe(true);
    expect(
      buttons.find({name: 'STAR'}).find('Icon[name="STAR"]').exists()
    ).toBe(true);
    expect(
      buttons.find({name: 'LOCATION'}).find('Icon[name="LOCATION"]').exists()
    ).toBe(true);
    expect(buttons.find({name: 'start'}).props().children).toBe('START');
  });
  test('selected avatars should be disabled', () => {
    expect(wrapper.find('Button[name="CIRCLE"]').props().disabled).toBe(false);
    expect(wrapper.find('Button[name="CROSS"]').props().disabled).toBe(false);
    expect(wrapper.find('Button[name="STAR"]').props().disabled).toBe(false);
    expect(wrapper.find('Button[name="LOCATION"]').props().disabled).toBe(false);
    wrapper.setProps({
      selectedAvatars: ['CROSS', 'LOCATION']
    });
    expect(wrapper.find('Button[name="CIRCLE"]').props().disabled).toBe(false);
    expect(wrapper.find('Button[name="CROSS"]').props().disabled).toBe(true);
    expect(wrapper.find('Button[name="STAR"]').props().disabled).toBe(false);
    expect(wrapper.find('Button[name="LOCATION"]').props().disabled).toBe(true);
  });
  test(`start button should be enabled only for the leader
      once all players have selected an avatar`, () => {
    // not leader and not all players have selected an avatar
    expect(wrapper.find({name: 'start'}).props().disabled).toBe(true);
    // not all players have selected an avatar
    wrapper.setProps({
      isLeader: true
    });
    expect(wrapper.find({name: 'start'}).props().disabled).toBe(true);

    wrapper.setProps({
      selectedAvatars: ['CROSS', 'CIRCLE']
    });
    expect(wrapper.find({name: 'start'}).props().disabled).toBe(false);
  });
});
