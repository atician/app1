import React from 'react';
import styled from 'styled-components';
import { bool, func, arrayOf } from 'prop-types';

import Icon from '../icon/Icon';
import Button from '../utils/Button';
import * as icons from '../constants/icons';
import { avatarType } from '../constants/propTypes';
import { MAX_PLAYERS } from '../constants/gameConstants';

const AvatarChooser = ({
  isLeader,
  selectedAvatars,
  handleAvatar,
  handleStart
}) => (
  <AvatarChooserWrapper>
    <Avatars>
      {Object.keys(icons).map((icon) => {
        return (
          <Button
            key={icon}
            name={icon}
            disabled={selectedAvatars.includes(icon)}
            onClick={handleAvatar}
            round>
            <Icon name={icon}/>
          </Button>
        );
      })}
    </Avatars>
    <Button
      name="start"
      disabled={!isLeader || selectedAvatars.length !== MAX_PLAYERS}
      onClick={handleStart}>
      START
    </Button>
  </AvatarChooserWrapper>
);
AvatarChooser.propTypes = {
  isLeader: bool.isRequired,
  selectedAvatars: arrayOf(avatarType).isRequired,
  handleAvatar: func.isRequired,
  handleStart: func.isRequired
};
const Avatars = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-evenly;
  width: 100%;
`;
const AvatarChooserWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-around;
  align-items: center;
  width: 60vw;
  height: 60vw;
  max-width: 250px;
  max-height: 250px;
`;

export default AvatarChooser;
