import { connect } from 'react-redux';

import App from './App';
import { SINGLE_PLAYER } from '../constants/gameConstants';

export const mapStateToProps = ({ mode, room, owner, isLoading, error }) => ({
  hasJoinedGame: Boolean((mode === SINGLE_PLAYER && owner) || room),
  isLoading,
  error
});
export default connect(
  mapStateToProps
)(App);
