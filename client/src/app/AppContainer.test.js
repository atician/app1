import { mapStateToProps } from './AppContainer';
import { SINGLE_PLAYER, MULTI_PLAYER } from '../constants/gameConstants';

describe('AppContainer mapStateToProps', () => {
  test('hasJoinedGame should be true for single player if player has joined', () => {
    // player not joined
    expect(mapStateToProps({
      mode: SINGLE_PLAYER,
      owner: null
    }).hasJoinedGame).toBe(false);
    // player joined
    expect(mapStateToProps({
      mode: SINGLE_PLAYER,
      owner: 'player_1'
    }).hasJoinedGame).toBe(true);
  });
  test('hasJoinedGame should be true for non single player if room is set', () => {
    expect(mapStateToProps({
      mode: MULTI_PLAYER,
      room: null
    }).hasJoinedGame).toBe(false);
    expect(mapStateToProps({
      mode: MULTI_PLAYER,
      room: 'room_1'
    }).hasJoinedGame).toBe(true);
  });
});
