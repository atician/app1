import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
import HomePageContainer from '../homePage/HomePageContainer';
import GamePageContainer from '../gamePage/GamePageContainer';

Enzyme.configure({adapter: new Adapter()});

describe('should render App', () => {
  test('should render home page if player has not joined', () => {
    const wrapper = shallow(<App hasJoinedGame={false}/>);
    expect(wrapper.find(HomePageContainer).exists()).toBe(true);
    expect(wrapper.find(GamePageContainer).exists()).toBe(false);
  });
  test('should render game page if player has joined', () => {
    const wrapper = shallow(<App hasJoinedGame={true}/>);
    expect(wrapper.find(GamePageContainer).exists()).toBe(true);
    expect(wrapper.find(HomePageContainer).exists()).toBe(false);
  });
});
