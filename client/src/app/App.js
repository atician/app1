import React from 'react';
import styled from 'styled-components';
import { bool } from 'prop-types';

import HomePageContainer from '../homePage/HomePageContainer';
import GamePageContainer from '../gamePage/GamePageContainer';

const App = ({ hasJoinedGame, isLoading, error }) => (
  <AppWrapper>
    {hasJoinedGame ? <GamePageContainer/> : <HomePageContainer/>}
  </AppWrapper>
);
App.propTypes = {
  hasJoinedGame: bool.isRequired
};
const AppWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  width: 95%;
  height: 95%;
  max-width: 700px;
  max-height: 600px;
  border-radius: 10px;
  box-shadow: 10px 10px 30px;
  background: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNSIgaGVpZ2h0PSIxNSI+CjxyZWN0IHdpZHRoPSI1MCIgaGVpZ2h0PSI1MCIgZmlsbD0iIzI4MjgyOCI+PC9yZWN0Pgo8Y2lyY2xlIGN4PSIzIiBjeT0iNC4zIiByPSIxLjgiIGZpbGw9IiMzOTM5MzkiPjwvY2lyY2xlPgo8Y2lyY2xlIGN4PSIzIiBjeT0iMyIgcj0iMS44IiBmaWxsPSJibGFjayI+PC9jaXJjbGU+CjxjaXJjbGUgY3g9IjEwLjUiIGN5PSIxMi41IiByPSIxLjgiIGZpbGw9IiMzOTM5MzkiPjwvY2lyY2xlPgo8Y2lyY2xlIGN4PSIxMC41IiBjeT0iMTEuMyIgcj0iMS44IiBmaWxsPSJibGFjayI+PC9jaXJjbGU+Cjwvc3ZnPg==");
`;

export default App;
