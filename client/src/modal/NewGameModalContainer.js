import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import { bool } from 'prop-types';

import NewGameModal from './NewGameModal';
import { resetGame } from '../actions/actions';
import { handleReset, addSocketListener } from '../actions/actionHelpers';
import { SINGLE_PLAYER } from '../constants/gameConstants';
import { EVENT_RESET } from '../constants/actionTypes';

class NewGameModalContainer extends Component {
  componentDidMount() {
    Modal.setAppElement('#root');
    this.props.dispatch(addSocketListener(EVENT_RESET, resetGame));
  }
  render() {
    return (
      <Modal
        isOpen={this.props.showModal}
        className="modal"
        overlayClassName="overlay"
        contentLabel="Minimal Modal Example">
        <NewGameModal {...this.props}/>
      </Modal>
    );
  }
}
NewGameModalContainer.propTypes = {
  showModal: bool.isRequired,
  ...NewGameModal.propTypes
};
const mapStateToProps = ({ mode, owner, players, winner, moves }) => {
  const isTie = moves.filter(move => !move).length === 0 && !winner;
  const leader = winner ? winner : players.ids[0];
  return {
    ...players.byId[owner],
    status: isTie ? 0 : winner === owner ? 1 : -1,
    isLeader: mode === SINGLE_PLAYER || owner === leader,
    showModal: !!winner || isTie
  };
};
const mapDispatchToProps = dispatch => ({
  dispatch,
  handleReset: () => dispatch(handleReset())
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewGameModalContainer);
