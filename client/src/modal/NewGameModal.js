import React from 'react';
import styled from 'styled-components';
import { bool, number, string, func } from 'prop-types';

import Button from '../utils/Button';
import PlayerDetails from '../utils/PlayerDetails';


const NewGameModal = ({
  name,
  avatar,
  score,
  status,
  isLeader,
  handleReset
}) => (
  <NewGameModalWrapper>
    <Header>{status ? (status > 0 ? 'YOU WIN' : 'YOU LOSE') : 'TIE'}</Header>
    <PlayerDetails
      lg
      name={name}
      avatar={avatar}
      score={status > 0 ? score + 1 : score}/>
    <Button onClick={handleReset} disabled={!isLeader}>
      NEW GAME
    </Button>
  </NewGameModalWrapper>
);
NewGameModal.propTypes = {
  name: string.isRequired,
  avatar: string.isRequired,
  score: number.isRequired,
  status: number.isRequired,
  isLeader: bool.isRequired,
  handleReset: func.isRequired
};
const NewGameModalWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  align-items: center;
`;
const Header = styled.span`
  font-size: 1.5em;
`;
export default NewGameModal;
