import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import HomePage from './HomePage';
import Input from '../utils/Input';
import Button from '../utils/Button';
import { SINGLE_PLAYER, MULTI_PLAYER } from '../constants/gameConstants';

Enzyme.configure({adapter: new Adapter()});

let props, wrapper;

describe('HomePage', () => {
  beforeEach(() => {
    props = {
      name: '',
      mode: null,
      handleName: jest.fn(),
      handleMode: jest.fn(),
      handleJoin: jest.fn()
    };
    wrapper = shallow(<HomePage {...props}/>);
  });
  test('should render HomePage', () => {
    // expect name input field to have placeholder
    const inputProps = wrapper.find(Input).props();
    expect(inputProps.type).toBe('text');
    expect(inputProps.placeholder).toBe('NAME');
    // expect single player, multi player and join buttons to be present
    expect(wrapper.find({name: SINGLE_PLAYER}).props().children).toBe('SINGLE PLAYER');
    expect(wrapper.find({name: MULTI_PLAYER}).props().children).toBe('MULTI PLAYER');
    expect(wrapper.find({name: 'join'}).props().children).toBe('JOIN');
  });
  describe('name input', () => {
    test('should display value set in name prop', () => {
      expect(wrapper.find(Input).props().value).toBe('');
      wrapper.setProps({name: 'Joe'});
      expect(wrapper.find(Input).props().value).toBe('Joe');
    });
    test('should call handleName on name input change', () => {
      wrapper.find(Input).simulate('change');
      expect(props.handleName).toHaveBeenCalledTimes(1);
    });
  });
  describe('single player button', () => {
    test('should call handleMode on game mode selection', () => {
      const singlePlayerButton = wrapper.find({name: SINGLE_PLAYER});
      singlePlayerButton.simulate('click');
      expect(props.handleMode).toHaveBeenCalledTimes(1);
    });
    test('should be disabled if no name is set', () => {
      expect(wrapper.find({name: SINGLE_PLAYER}).props().disabled).toBe(true);
      wrapper.setProps({name: 'Joe'});
      expect(wrapper.find({name: SINGLE_PLAYER}).props().disabled).toBe(false);
    });
    test('should be active if mode is SINGLE_PLAYER', () => {
      expect(wrapper.find({name: SINGLE_PLAYER}).props().active).toBe(false);
      wrapper.setProps({mode: MULTI_PLAYER});
      expect(wrapper.find({name: SINGLE_PLAYER}).props().active).toBe(false);
      wrapper.setProps({mode: SINGLE_PLAYER});
      expect(wrapper.find({name: SINGLE_PLAYER}).props().active).toBe(true);
    });
  });
  describe('multi player button', () => {
    test('should call handleMode on game mode selection', () => {
      const singlePlayerButton = wrapper.find({name: MULTI_PLAYER});
      singlePlayerButton.simulate('click');
      expect(props.handleMode).toHaveBeenCalledTimes(1);
    });
    test('should be disabled if no name is set', () => {
      expect(wrapper.find({name: MULTI_PLAYER}).props().disabled).toBe(true);
      wrapper.setProps({name: 'Joe'});
      expect(wrapper.find({name: MULTI_PLAYER}).props().disabled).toBe(false);
    });
    test('should be active if mode is MULTI_PLAYER', () => {
      expect(wrapper.find({name: MULTI_PLAYER}).props().active).toBe(false);
      wrapper.setProps({mode: SINGLE_PLAYER});
      expect(wrapper.find({name: MULTI_PLAYER}).props().active).toBe(false);
      wrapper.setProps({mode: MULTI_PLAYER});
      expect(wrapper.find({name: MULTI_PLAYER}).props().active).toBe(true);
    });
  });
  describe('join button', () => {
    test('should call handleJoin on game join', () => {
      const joinButton = wrapper.find({name: 'join'});
      joinButton.simulate('click');
      expect(props.handleJoin).toHaveBeenCalledTimes(1);
    });
    test('should only be enabled if both name and mode are set', () => {
      // expect button to be disabled if no name or mode is set
      expect(wrapper.find({name: 'join'}).props().disabled).toBe(true);
      // expect button to be disabled if no mode is set
      wrapper.setProps({
        name: 'Joe'
      });
      expect(wrapper.find({name: 'join'}).props().disabled).toBe(true);
      // expect button to be disabled if no name is set
      wrapper.setProps({
        name: '',
        mode: SINGLE_PLAYER
      });
      expect(wrapper.find({name: 'join'}).props().disabled).toBe(true);
      // expect button to be enabled if both name and mode are set
      wrapper.setProps({
        name: 'Joe',
        mode: SINGLE_PLAYER
      });
      expect(wrapper.find({name: 'join'}).props().disabled).toBe(false);
    });
  });
});
