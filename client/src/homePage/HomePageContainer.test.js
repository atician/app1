import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { HomePageContainer, mapDispatchToProps } from './HomePageContainer';
import HomePage from './HomePage'
import * as actionHelpers from '../actions/actionHelpers';
import { SINGLE_PLAYER } from '../constants/gameConstants';

Enzyme.configure({adapter: new Adapter()});
jest.mock('../actions/actionHelpers');

let props, wrapper;

describe('HomePageContainer', () => {
  beforeEach(() => {
    props = {
      mode: null,
      handleMode: jest.fn(),
      handleJoin: jest.fn(),
      dispatch: jest.fn()
    };
    actionHelpers._initMockHandleMode({type: null});
    actionHelpers._initMockHandleJoin({type: null});
    wrapper = shallow(<HomePageContainer {...props}/>);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('should render HomePageContainer', () => {
    expect(wrapper.find(HomePage).exists()).toBe(true);
  });
  test('should update name on name change', () => {
    expect(wrapper.props().name).toBe('');
    wrapper.props().handleName({target: {value: 'Joe'}});
    // should convert name to uppercase
    expect(wrapper.props().name).toBe('JOE');
  });
  test('should limit name length', () => {
    wrapper.props().handleName({target: {value: 'JoeHasALon'}});
    // should convert name to uppercase
    expect(wrapper.props().name).toBe('JOEHASALON');
    wrapper.props().handleName({target: {value: 'JoeHasALongName'}});
    expect(wrapper.props().name).toBe('JOEHASALON');
  });
  test('should update mode on mode change', () => {
    // expect handleMode to call props.handleMode
    wrapper.props().handleMode();
    expect(props.handleMode).toHaveBeenCalledTimes(1);
    // expect mapDispatchToProps' handleMode to be called with the right args
    mapDispatchToProps(props.dispatch).handleMode({target: {name: SINGLE_PLAYER}});
    expect(actionHelpers.handleMode).toHaveBeenCalledTimes(1);
    expect(actionHelpers.handleMode).toHaveBeenCalledWith(SINGLE_PLAYER);
  });
  test('should call handleJoin with name', () => {
    // expect handleJoin to call props.handleJoin
    wrapper.props().handleJoin();
    expect(props.handleJoin).toHaveBeenCalledTimes(1);
    expect(props.handleJoin).toHaveBeenCalledWith('');
    props.handleJoin.mockClear();
    // expect handleJoin to call props.handleJoin with name
    wrapper.setState({name: 'JOE'});
    wrapper.props().handleJoin();
    expect(props.handleJoin).toHaveBeenCalledTimes(1);
    expect(props.handleJoin).toHaveBeenCalledWith('JOE');
    // expect mapDispatchToProps' handleJoin to be called with the right args
    mapDispatchToProps(props.dispatch).handleJoin('');
    expect(actionHelpers.handleJoin).toHaveBeenCalledTimes(1);
    expect(actionHelpers.handleJoin).toHaveBeenCalledWith('');
    actionHelpers.handleJoin.mockClear();

    mapDispatchToProps(props.dispatch).handleJoin('JOE');
    expect(actionHelpers.handleJoin).toHaveBeenCalledTimes(1);
    expect(actionHelpers.handleJoin).toHaveBeenCalledWith('JOE');
    actionHelpers.handleJoin.mockClear();
    // expect spaces to be trimmed
    mapDispatchToProps(props.dispatch).handleJoin('   ');
    expect(actionHelpers.handleJoin).toHaveBeenCalledTimes(1);
    expect(actionHelpers.handleJoin).toHaveBeenCalledWith('');
  });
});
