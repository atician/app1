import React, { Component } from 'react';
import { connect } from 'react-redux';
import { func } from 'prop-types';

import HomePage from './HomePage';
import { handleMode, handleJoin } from '../actions/actionHelpers';
import { modeType } from '../constants/propTypes';
import { MAX_NAME_LENGTH } from '../constants/gameConstants';

export class HomePageContainer extends Component {
  constructor(props) {
    super(props);
    this.handleName = this.handleName.bind(this);
    this.handleJoin = this.handleJoin.bind(this);
    this.state = {
      name: ''
    };
  }
  handleName(e) {
    const name = e.target.value.toUpperCase();
    if (name.length > MAX_NAME_LENGTH) {
      return;
    }
    this.setState({ name });
  }
  handleJoin() {
    this.props.handleJoin(this.state.name);
  }
  render() {
    return (
      <HomePage
        name={this.state.name}
        mode={this.props.mode}
        handleName={this.handleName}
        handleMode={this.props.handleMode}
        handleJoin={this.handleJoin}/>
    );
  }
}
HomePageContainer.propTypes = {
  mode: modeType,
  handleMode: func.isRequired,
  handleJoin: func.isRequired
};
const mapStateToProps = ({ mode }) => ({
  mode
});
export const mapDispatchToProps = dispatch => ({
  handleMode: e => dispatch(handleMode(e.target.name)),
  handleJoin: name => dispatch(handleJoin(name.trim()))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePageContainer);
