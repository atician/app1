import React from 'react';
import styled from 'styled-components';
import { string, func } from 'prop-types';

import Button from '../utils/Button';
import Input from '../utils/Input';
import { SINGLE_PLAYER, MULTI_PLAYER } from '../constants/gameConstants';
import { modeType } from '../constants/propTypes';

const HomePage = ({
  name,
  mode,
  handleName,
  handleMode,
  handleJoin
}) => (
  <HomePageWrapper>
    <Input
      type="text"
      value={name}
      placeholder="NAME"
      onChange={handleName}
    />
    <Button
      active={mode === SINGLE_PLAYER}
      name={SINGLE_PLAYER}
      disabled={!name}
      onClick={handleMode}>
        SINGLE PLAYER
    </Button>
    <Button
      active={mode === MULTI_PLAYER}
      name={MULTI_PLAYER}
      disabled={!name}
      onClick={handleMode}>
        MULTI PLAYER
    </Button>
    <Button
      name="join"
      disabled={!name || !mode}
      onClick={handleJoin}>
        JOIN
    </Button>
  </HomePageWrapper>
);
HomePage.propTypes = {
  name: string.isRequired,
  mode: modeType,
  handleName: func.isRequired,
  handleMode: func.isRequired,
  handleJoin: func.isRequired
};
const HomePageWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  align-items: center;
  flex: 1;
  height: 100%;
`;

export default HomePage;
