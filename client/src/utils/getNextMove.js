import getWinner from './getWinner';
// heuristic value for game tree leaf node
const BASE_SCORE = 100;
/*
  0 | 1 | 2
  ---------
  3 | 4 | 5
  ---------
  6 | 7 | 8

  0 | 1 | 2 | 3
  --------------
  4 | 5 | 6 | 7
  --------------
  8 | 9 | 10| 11
  --------------
  12| 13| 14| 15
*/
const alphaBeta = (
    maximizer,
    minimizer,
    moves,
    initAlpha,
    initBeta,
    isMaximizer,
    depth = 0
) => {
  const player = isMaximizer ? maximizer : minimizer;
  let score = isMaximizer ? Number.NEGATIVE_INFINITY : Number.POSITIVE_INFINITY,
      alpha = initAlpha,
      beta = initBeta,
      nextMove;
  moves.some((move, position) => {
    if (move) {
      return false;
    }
    if (getWinner(position, player, moves) === player) {
      score = isMaximizer ? BASE_SCORE - depth : -BASE_SCORE + depth;
      if (depth === 0 && score > alpha) {
        nextMove = position;
      }
      return true;
    } else if (moves.filter(move => !move).length === 1) {
      score = 0;
      if (depth === 0 && score > alpha) {
        nextMove = position;
      }
      return true;
    } else if (isMaximizer) {
      const newMoves = moves.slice();
      newMoves[position] = player;
      score = Math.max(score, alphaBeta(maximizer, minimizer, newMoves,
          alpha, beta, !isMaximizer, depth + 1));
      if (depth === 0 && score > alpha) {
        nextMove = position;
      }
      alpha = Math.max(alpha, score);
      if (alpha >= beta) {
        return true;
      }
    } else {
      const newMoves = moves.slice();
      newMoves[position] = player;
      score = Math.min(score, alphaBeta(maximizer, minimizer, newMoves,
          alpha, beta, !isMaximizer, depth + 1));
      beta = Math.min(beta, score);
      if (alpha >= beta) {
        return true;
      }
    }
    return false;
  });
  return depth === 0 ? nextMove : score;
};
const getNextMove = (player, opponent, moves) => {
  return alphaBeta(
      player, opponent, moves,
      Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, true);
};
export default getNextMove;
