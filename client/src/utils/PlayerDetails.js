import React from 'react';
import styled from 'styled-components';
import { string, number, bool } from 'prop-types';

import Avatar from '../avatar/Avatar';
import { avatarType } from '../constants/propTypes';

const PlayerDetails = ({
  name,
  avatar,
  score,
  active,
  sm,
  md,
  lg
}) => (
  <PlayerDetailsWrapper active={active} sm={sm} md={md} lg={lg}>
    <Avatar name={avatar} sm={sm} md={md} lg={lg}/>
    <Details>
        {name}
      <Score>
        SCORE: {score}
      </Score>
    </Details>
  </PlayerDetailsWrapper>
);
PlayerDetails.propTypes = {
  name: string.isRequired,
  avatar: avatarType,
  score: number.isRequired,
  active: bool,
  sm: bool,
  md: bool,
  lg: bool
};
const PlayerDetailsWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  align-items: center;
  border-radius: 5px;
  width: 100%;
  padding: 5px;
  background-color: ${props => props.active ? 'green' : ''};
  box-shadow: ${props => props.active ? '5px 5px 10px grey' : 'none'};
`;
const Details = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: flex-start;
  margin-left: 10px;
`;

const Score = styled.div`
  display: flex;
  flex-flow: row nowrap;
`;
export default PlayerDetails;
