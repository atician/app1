import { MOVES_TO_WIN } from '../constants/gameConstants';
/*
  0 | 1 | 2
  ---------
  3 | 4 | 5
  ---------
  6 | 7 | 8

  0 | 1 | 2 | 3
  --------------
  4 | 5 | 6 | 7
  --------------
  8 | 9 | 10| 11
  --------------
  12| 13| 14| 15
*/
const isPlayerInPosition = (player, moves) => position => (
    position >= 0
    && position < moves.length
    && typeof moves[position] === 'string'
    && moves[position] === player
);
const searchPlayer = (position, dx, dy, size, hasFoundPlayer, depth = 0) => {
  if (depth >= size - 1) {
    return depth;
  } else if (position % size === 0 && dx === -1) {
    return depth;
  } else if ((position + 1) % size === 0 && dx === 1) {
    return depth;
  }
  const nextPosition = position + dx + dy;
  if (hasFoundPlayer(nextPosition)) {
    return searchPlayer(nextPosition, dx, dy, size, hasFoundPlayer, depth + 1);
  } else {
    return depth;
  }
};
const getWinner = (position, player, moves) => {
  const size = Math.sqrt(moves.length);
  const hasFoundPlayer = isPlayerInPosition(player, moves);
  const deltas = [
    {dx: 0, dy: size},
    {dx: 1, dy: 0},
    {dx: 1, dy: size},
    {dx: -1, dy: size}
  ];
  const hasWinner = deltas.some(({ dx, dy }) => {
    const depth = searchPlayer(position, dx, dy, size, hasFoundPlayer)
        + searchPlayer(position, -dx, -dy, size, hasFoundPlayer);
    // depth is +1 as the player's most recent move is included with getWinner()
    return depth + 1 === MOVES_TO_WIN;
  });
  return hasWinner ? player : null;
};
export default getWinner;
