import React from 'react';
import styled from 'styled-components';
import { bool } from 'prop-types';

import { FLEX, BOX_SHADOW, SIZE, BACKGROUND_COLOR } from '../constants';

export default function Button(props) {
  return (
    <ButtonWrapper {...props}>
      {props.children}
    </ButtonWrapper>
  );
}
Button.propTypes = {
  active: bool,
  disabled: bool,
  round: bool,
  sm: bool,
  md: bool,
  lg: bool
};
const ButtonWrapper = styled.button`
  ${FLEX};
  ${BOX_SHADOW};
  ${SIZE};
  ${BACKGROUND_COLOR};
  padding: ${props => props.round ?  '' : '15px 20px'};
  opacity: ${props => props.disabled ? 0.5 : 1};
  border-radius: 100px;
  border: 0;
  outline: none;
  transition: all 0.5s;
  font-weight: bold;
  font-size: 1em;
  font-family: 'Jua', sans-serif;
  &:hover {
    cursor: pointer;
  }
  &:disabled {
    cursor: default;
  }
`;
