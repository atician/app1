import getWinner from './getWinner';
/*
  0 | 1 | 2
  ---------
  3 | 4 | 5
  ---------
  6 | 7 | 8

  0 | 1 | 2 | 3
  --------------
  4 | 5 | 6 | 7
  --------------
  8 | 9 | 10| 11
  --------------
  12| 13| 14| 15
*/
describe('getWinner tests', () => {
  const player1 = 'PDP-PY2jJ8iqfIIBAAAL',
        player2 = 'mGOdHK0B_r7ae0HJAAAJ';
  let position;
  let moves;
  const testAllPositions = (winner, winningPosition, moves) => {
    moves.forEach((player, position) => {
      if (player) {
        return;
      } else if (position === winningPosition) {
        expect(getWinner(position, winner, moves)).toBe(winner);
      } else {
        expect(getWinner(position, winner, moves)).toBeNull();
      }
    });
  };
  beforeEach(() => {
    moves = Array(9).fill(null);
  });
  describe('win only at position 0', () => {
    beforeEach(() => {
      position = 0;
    });
    test('case 0: east', () => {
      moves[1] = moves[2] = player1;
      testAllPositions(player1, position, moves);
    });
    test('case 1: south', () => {
      moves[3] = moves[6] = player1;
      testAllPositions(player1, position, moves);
    });
    test('case 2: south east', () => {
      moves[4] = moves[8] = player1;
      testAllPositions(player1, position, moves);
    });
  });
  describe('win only at position 4', () => {
    beforeEach(() => {
      position = 4;
    });
    test('case 0: north to south', () => {
      moves[0] = moves[8] = player1;
      testAllPositions(player1, position, moves);
    });
    test('case 1: east to west', () => {
      moves[3] = moves[5] = player1;
      testAllPositions(player1, position, moves);
    });
    test('case 2: north east to south west', () => {
      moves[2] = moves[6] = player1;
      testAllPositions(player1, position, moves);
    });
    test('case 3: north west to south east', () => {
      moves[0] = moves[8] = player1;
      testAllPositions(player1, position, moves);
    });
    test('case 4: north south east west', () => {
      moves[1] = moves[3] = moves[5] = moves[7] = player1;
      testAllPositions(player1, position, moves);
    });
  });
  describe('directional wins', () => {
    test('case 0: vertical win', () => {
      moves[0] = moves[6] = player2;
      moves[1] = player1;
      testAllPositions(player2, 3, moves);
    });
  });
});
