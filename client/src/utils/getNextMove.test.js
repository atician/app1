import getNextMove from './getNextMove';
/*
  0 | 1 | 2
  ---------
  3 | 4 | 5
  ---------
  6 | 7 | 8

  0 | 1 | 2 | 3
  --------------
  4 | 5 | 6 | 7
  --------------
  8 | 9 | 10| 11
  --------------
  12| 13| 14| 15
*/
describe('getNextMove tests', () => {
  const o = 'o_player_1',
        x = 'x_player_2';
  describe('test 0: near end game state', () => {
    test('case 0: prevent opponent from winning and win yourself', () => {
      const moves = [
        o, x, o,
        x, null, x,
        o, x, o
      ];
      expect(getNextMove(o, x, moves)).toBe(4);
      expect(getNextMove(x, o, moves)).toBe(4);
    });
    test('case 1: prevent opponent from winning', () => {
      const moves = [
        o, x, o,
        o, x, x,
        x, null, o
      ];
      expect(getNextMove(o, x, moves)).toBe(7);
      expect(getNextMove(x, o, moves)).toBe(7);
    });
    test('case 2: prevent opponent from winning', () => {
      const moves = [
        o, x, o,
        null, x, o,
        x, null, x
      ];
      expect(getNextMove(o, x, moves)).toBe(7);
      expect(getNextMove(x, o, moves)).toBe(7);
    });
    test('case 3: prevent opponent from winning', () => {
      const moves = [
        o, null, null,
        o, null, x,
        x, o, x
      ];
      expect(getNextMove(o, x, moves)).toBe(2);
      expect(getNextMove(x, o, moves)).toBe(2);
    });
    test('case 4: best defense is offense', () => {
      const moves = [
        x, o, x,
        o, null, o,
        x, null, x
      ];
      expect(getNextMove(o, x, moves)).toBe(4);
    });
  });
  describe('test 1: perform winning move', () => {
    test('case 0: horizontal win', () => {
      const moves = [
        null, o, o,
        x, null, x,
        null, null, null
      ];
      expect(getNextMove(o, x, moves)).toBe(0);
      expect(getNextMove(x, o, moves)).toBe(4);
    });
    test('case 1: vertical win', () => {
      const moves = [
        null, null, x,
        null, o, null,
        null, o, x
      ];
      expect(getNextMove(o, x, moves)).toBe(1);
      expect(getNextMove(x, o, moves)).toBe(5);
    });
    test('case 2: diagonal win', () => {
      const moves = [
        o, null, null,
        null, null, null,
        null, null, o
      ];
      expect(getNextMove(o, x, moves)).toBe(4);
    });
    test('case 3: diagonal win', () => {
      const moves = [
        null, null, o,
        null, o, null,
        null, null, null
      ];
      expect(getNextMove(o, x, moves)).toBe(6);
    });
  });
  describe('test 2: prevent opponent from winning', () => {
    test('case 0: prevent horizontal win', () => {
      const moves = [
        x, null, x,
        null, o, null,
        null, null, null
      ];
      expect(getNextMove(o, x, moves)).toBe(1);
      expect(getNextMove(x, o, moves)).toBe(1);
    });
    test('case 1: prevent horizontal win', () => {
      const moves = [
        o, null, null,
        x, x, null,
        null, null, null
      ];
      expect(getNextMove(o, x, moves)).toBe(5);
      expect(getNextMove(x, o, moves)).toBe(5);
    });
    test('case 2: prevent vertical win', () => {
      const moves = [
        x, o, null,
        null, null, null,
        x, null, null
      ];
      expect(getNextMove(o, x, moves)).toBe(3);
      expect(getNextMove(x, o, moves)).toBe(3);
    });
    test('case 3: prevent vertical win', () => {
      const moves = [
        null, null, o,
        null, x, null,
        null, x, null
      ];
      expect(getNextMove(o, x, moves)).toBe(1);
      expect(getNextMove(x, o, moves)).toBe(1);
    });
    test('case 4: prevent diagonal win', () => {
      const moves = [
        x, null, o,
        null, null, null,
        null, null, x
      ];
      expect(getNextMove(o, x, moves)).toBe(4);
      expect(getNextMove(x, o, moves)).toBe(4);
    });
    test('case 5: prevent diagonal win', () => {
      const moves = [
        o, null, x,
        null, x, null,
        null, null, null
      ];
      expect(getNextMove(o, x, moves)).toBe(6);
      expect(getNextMove(x, o, moves)).toBe(6);
    });
  });
  describe('test 3: perform valid move', () => {
    test('case 0: empty board', () => {
      const moves = [
        null, null, null,
        null, null, null,
        null, null, null
      ];
      const nextMove = getNextMove(o, x, moves);
      expect(nextMove).toBeGreaterThanOrEqual(0);
      expect(nextMove).toBeLessThan(9);
    });
    test('case 1: second move', () => {
      const moves = [
        null, null, null,
        null, x, null,
        null, null, null
      ];
      expect(getNextMove(o, x, moves)).not.toBe(4);
    });
    test('case 2: second move', () => {
      const moves = [
        x, null, null,
        null, null, null,
        null, null, null
      ];
      expect(getNextMove(o, x, moves)).not.toBe(0);
    });
    test('case 3: second move', () => {
      const moves = [
        null, null, null,
        null, null, null,
        null, x, null
      ];
      expect(getNextMove(o, x, moves)).not.toBe(7);
    });
    test('case 4: third move', () => {
      const moves = [
        o, null, null,
        null, null, null,
        x, null, null
      ];
      const nextMove = getNextMove(o, x, moves);
      expect(nextMove).not.toBe(3);
      expect(nextMove).not.toBe(5);
      expect(nextMove).not.toBe(7);
      expect(nextMove).not.toBe(6);
    });
    test('case 5: third move', () => {
      const moves = [
        null, null, null,
        null, o, null,
        x, null, null
      ];
      const nextMove = getNextMove(o, x, moves);
      expect(nextMove).not.toBe(2);
      expect(nextMove).not.toBe(6);
    });
  });
  describe('test 4: rigged game to give opponent an advantage', () => {
    test('case 0: prevent opponent from winning', () => {
      const moves = [
        x, null, o,
        null, null, null,
        null, null, x
      ];
      expect(getNextMove(o, x, moves)).toBe(4);
      expect(getNextMove(x, o, moves)).toBe(4);
    });
    test('case 1: prevent opponent from winning and win yourself', () => {
      const moves = [
        x, null, o,
        null, o, null,
        null, x, x
      ];
      expect(getNextMove(o, x, moves)).toBe(6);
      expect(getNextMove(x, o, moves)).toBe(6);
    });
    test('case 2: prevent opponent from winning even if you will lose', () => {
      const moves = [
        x, null, x,
        o, x, null,
        o, null, null
      ];
      let nextMove = getNextMove(o, x, moves);
      expect(nextMove).not.toBe(5);
      expect(nextMove).not.toBe(7);
      nextMove = getNextMove(x, o, moves);
      expect(nextMove).not.toBe(5);
      expect(nextMove).not.toBe(7);
    });
    test('case 3: prevent opponent from winning even if you will lose', () => {
      const moves = [
        x, null, x,
        o, null, null,
        o, null, x
      ];
      let nextMove = getNextMove(o, x, moves);
      expect(nextMove).not.toBe(7);
      nextMove = getNextMove(x, o, moves);
      expect(nextMove).not.toBe(7);
    });
  });
});
