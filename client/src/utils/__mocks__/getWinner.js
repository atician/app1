let mockWinner;
export const _setMockWinner = winner => {
  mockWinner = winner;
};
export const getWinner = jest.fn(winner => mockWinner);
export default getWinner;
