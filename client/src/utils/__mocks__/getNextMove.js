
export const _initMockGetNextMove = position => {
  getNextMove.position = position;
}
export const getNextMove = jest.fn(() => getNextMove.position);
export default getNextMove;
