import React from 'react';
import styled from 'styled-components';
import { bool } from 'prop-types';

import { BOX_SHADOW, BACKGROUND_COLOR } from '../constants';

export default function Input(props) {
  return (
    <InputWrapper {...props}>
      {props.children}
    </InputWrapper>
  );
}
Input.propTypes = {
  active: bool,
  disabled: bool
};
const InputWrapper = styled.input`
  ${BOX_SHADOW};
  ${BACKGROUND_COLOR};
  padding: 15px 20px;
  border-radius: 100px;
  border: 0;
  outline: none;
  transition: all 0.5s;
  font-weight: bold;
  font-size: 1em;
  font-family: 'Jua', sans-serif;
  &:focus {
    box-shadow: none;
  }
  &:disabled {
    cursor: default;
  }
`;
