// Game modes
export const SINGLE_PLAYER = 'SINGLE_PLAYER';
export const MULTI_PLAYER = 'MULTI_PLAYER';

export const MAX_NAME_LENGTH = 10;
export const MAX_MESSAGE_LENGTH = 256;
export const MAX_PLAYERS = 2;
export const MAX_MOVES = 9;
export const MOVES_TO_WIN = 3;

export const PC_MOVE_DELAY = '300';
export const PC_PLAYER = {
  id: 'PC_PLAYER',
  name: 'PC',
  avatar: 'CROSS',
  score: 0
};
