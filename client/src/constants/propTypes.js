import { string, number, shape, arrayOf, oneOf } from 'prop-types';

import * as icons from './icons';
import { SINGLE_PLAYER, MULTI_PLAYER } from './gameConstants';

export const modeType = oneOf([SINGLE_PLAYER, MULTI_PLAYER]);
export const avatarType = oneOf(Object.keys(icons));
export const playersType = shape({
  byId: shape({
    id: string,
    name: string,
    avatar: avatarType,
    score: number
  }).isRequired,
  ids: arrayOf(string).isRequired
});
export const messagesType = shape({
  byId: shape({
    id: number,
    player: string,
    text: string
  }).isRequired,
  ids: arrayOf(number).isRequired
});
