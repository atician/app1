import { css } from 'styled-components';
import { rgba } from 'polished';

export const COLORS = {
  YELLOW: '#F9F871',
  GREEN: '#58C68A'
}
export const BOX_SHADOW = props => css`
  box-shadow: ${props.active || props.disabled ?
    'none' :
    `5px 5px 10px ${rgba(COLORS.YELLOW,0.2)}`
  };
  &:hover {
    box-shadow: none;
  }
`;
export const SIZE = props => css`
  width: ${props.round ? '40px' : ''};
  height: ${props.round ? '40px' : ''};
  ${props.sm ? `
    width: 30px;
    height: 30px;
  `: props.md ? `
    width: 50px;
    height: 50px;
  `: props.lg ? `
    width: 60px;
    height: 60px;
  `: ''};
`;
export const BACKGROUND_COLOR = props => css`
  background-color: ${props.active ? COLORS.GREEN : COLORS.YELLOW}
`;
export const FLEX = props => css`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
`;
