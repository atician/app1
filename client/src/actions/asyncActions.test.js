import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as asyncActions from './asyncActions';
import * as types from '../constants/actionTypes';

jest.mock('axios');
jest.mock('./actions');
import * as actions from './actions';
import axios from 'axios';

const mockStore = configureStore([thunk]);

describe('async actions', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  describe('postPlayer', () => {
    beforeEach(() => {
      actions._initSetRoom({type: types.POST_PLAYER_SUCCESS});
      actions._initSetError({type: types.POST_PLAYER_FAILURE});
    });
    test('should set room on successful post', () => {
      const store = mockStore();
      // mock post to resolve request
      axios._initPost({data: 'success'});
      return store.dispatch(asyncActions.postPlayer(
        {owner: 'player_1'},
        'Joe'
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_PLAYER_REQUEST,
            name: 'Joe'
          },
          {type: types.POST_PLAYER_SUCCESS}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/player/player_1',
          {name: 'Joe'}
        );
        // expect room to be set on successful post
        expect(actions.setRoom).toHaveBeenCalledTimes(1);
        expect(actions.setRoom).toHaveBeenCalledWith(
          'success',
          types.POST_PLAYER_SUCCESS
        );
      });
    });
    test('should set error on unsuccessful post', () => {
      const store = mockStore();
      // mock post to reject request
      axios._initPost({error: 'fail'});
      return store.dispatch(asyncActions.postPlayer(
        {owner: 'player_1'},
        'Joe'
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_PLAYER_REQUEST,
            name: 'Joe'
          },
          {type: types.POST_PLAYER_FAILURE}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/player/player_1',
          {name: 'Joe'}
        );
        // expect error to be set on unsuccessful post
        expect(actions.setError).toHaveBeenCalledTimes(1);
        expect(actions.setError).toHaveBeenCalledWith(
          {error: 'fail'},
          types.POST_PLAYER_FAILURE
        );
      });
    });
  });
  describe('getPlayers', () => {
    beforeEach(() => {
      actions._initSetPlayers({type: types.GET_PLAYER_SUCCESS});
      actions._initSetError({type: types.GET_PLAYER_FAILURE});
    });
    test('should set players on successful get', () => {
      const store = mockStore();
      // mock get to resolve request
      axios._initGet({data: 'success'});
      return store.dispatch(asyncActions.getPlayers(
        {
          room: 'room_1',
          owner: 'player_1'
        }
      )).then(() => {
        expect(store.getActions()).toEqual([
          {type: types.GET_PLAYER_REQUEST},
          {type: types.GET_PLAYER_SUCCESS}
        ]);
        expect(axios.get).toHaveBeenCalledTimes(1);
        expect(axios.get).toHaveBeenCalledWith('/api/room/room_1/player/player_1');
        // expect players to be set on successful get
        expect(actions.setPlayers).toHaveBeenCalledTimes(1);
        expect(actions.setPlayers).toHaveBeenCalledWith(
          'success',
          types.GET_PLAYER_SUCCESS
        );
      });
    });
    test('should set error on unsuccessful get', () => {
      const store = mockStore();
      // mock get to reject request
      axios._initGet({error: 'fail'});
      return store.dispatch(asyncActions.getPlayers(
        {
          room: 'room_1',
          owner: 'player_1'
        }
      )).then(() => {
        expect(store.getActions()).toEqual([
          {type: types.GET_PLAYER_REQUEST},
          {type: types.GET_PLAYER_FAILURE}
        ]);
        expect(axios.get).toHaveBeenCalledTimes(1);
        expect(axios.get).toHaveBeenCalledWith('/api/room/room_1/player/player_1');
        // expect error to be set on unsuccessful get
        expect(actions.setError).toHaveBeenCalledTimes(1);
        expect(actions.setError).toHaveBeenCalledWith(
          {error: 'fail'},
          types.GET_PLAYER_FAILURE
        );
      });
    });
  });
  describe('patchAvatar', () => {
    beforeEach(() => {
      actions._initMockSetAvatar({type: types.PATCH_AVATAR_SUCCESS});
      actions._initSetError({type: types.PATCH_AVATAR_FAILURE});
    });
    test('should update avatar on successful patch', () => {
      const store = mockStore();
      // mock patch to resolve request
      axios._initPatch({data: 'success'});
      return store.dispatch(asyncActions.patchAvatar(
        {
          room: 'room_1',
          owner: 'player_1'
        },
        'CROSS'
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.PATCH_AVATAR_REQUEST,
            owner: 'player_1',
            room: 'room_1',
            avatar: 'CROSS'
          },
          {type: types.PATCH_AVATAR_SUCCESS}
        ]);
        expect(axios.patch).toHaveBeenCalledTimes(1);
        expect(axios.patch).toHaveBeenCalledWith(
          '/api/room/room_1/player/player_1/avatar',
          {
            avatar: 'CROSS'
          }
        );
        // expect avatar to be updated on successful patch
        expect(actions.setAvatar).toHaveBeenCalledTimes(1);
        expect(actions.setAvatar).toHaveBeenCalledWith(
          'success',
          types.PATCH_AVATAR_SUCCESS
        );
      });
    });
    test('should set error on unsuccessful patch', () => {
      const store = mockStore();
      // mock patch to reject request
      axios._initPatch({error: 'fail'});
      return store.dispatch(asyncActions.patchAvatar(
        {
          room: 'room_1',
          owner: 'player_1'
        },
        'CROSS'
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.PATCH_AVATAR_REQUEST,
            owner: 'player_1',
            room: 'room_1',
            avatar: 'CROSS'
          },
          {type: types.PATCH_AVATAR_FAILURE}
        ]);
        expect(axios.patch).toHaveBeenCalledTimes(1);
        expect(axios.patch).toHaveBeenCalledWith(
          '/api/room/room_1/player/player_1/avatar',
          {
            avatar: 'CROSS'
          }
        );
        // expect error to be set on unsuccessful patch
        expect(actions.setError).toHaveBeenCalledTimes(1);
        expect(actions.setError).toHaveBeenCalledWith(
          {error: 'fail'},
          types.PATCH_AVATAR_FAILURE
        );
      });
    });
  });
  describe('postStart', () => {
    beforeEach(() => {
      actions._initMockStartGame({type: types.POST_START_SUCCESS});
      actions._initSetError({type: types.POST_START_FAILURE});
    });
    test('should start game on successful post', () => {
      const store = mockStore();
      // mock post to resolve request
      axios._initPost({data: 'success'});
      return store.dispatch(asyncActions.postStart(
        {
          owner: 'player_1',
          room: 'room_1'
        }
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_START_REQUEST,
            player: 'player_1'
          },
          {type: types.POST_START_SUCCESS}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/room/room_1/start',
          {player: 'player_1'}
        );
        // expect game to start on successful post
        expect(actions.startGame).toHaveBeenCalledTimes(1);
        expect(actions.startGame).toHaveBeenCalledWith(
          'success',
          types.POST_START_SUCCESS,
          {
            owner: 'player_1',
            room: 'room_1'
          }
        );
      });
    });
    test('should set error on unsuccessful post', () => {
      const store = mockStore();
      // mock post to reject request
      axios._initPost({error: 'fail'});
      return store.dispatch(asyncActions.postStart(
        {
          owner: 'player_1',
          room: 'room_1'
        }
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_START_REQUEST,
            player: 'player_1'
          },
          {type: types.POST_START_FAILURE}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/room/room_1/start',
          {player: 'player_1'}
        );
        // expect error to be set on unsuccessful post
        expect(actions.setError).toHaveBeenCalledTimes(1);
        expect(actions.setError).toHaveBeenCalledWith(
          {error: 'fail'},
          types.POST_START_FAILURE
        );
      });
    });
  });
  describe('postMove', () => {
    beforeEach(() => {
      actions._initMockPerformMove({type: types.POST_MOVE_SUCCESS});
      actions._initSetError({type: types.POST_MOVE_FAILURE});
    });
    test('should set move on successful post', () => {
      const store = mockStore();
      // mock post to resolve request
      axios._initPost({data: 'success'});
      return store.dispatch(asyncActions.postMove(
        {
          owner: 'player_1',
          room: 'room_1'
        },
        0
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_MOVE_REQUEST,
            player: 'player_1',
            position: 0
          },
          {type: types.POST_MOVE_SUCCESS}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/room/room_1/move',
          {
            player: 'player_1',
            position: 0
          }
        );
        // expect move to be set on successful post
        expect(actions.performMove).toHaveBeenCalledTimes(1);
        expect(actions.performMove).toHaveBeenCalledWith(
          'success',
          types.POST_MOVE_SUCCESS,
          {
            owner: 'player_1',
            room: 'room_1'
          }
        );
      });
    });
    test('should set error on unsuccessful post', () => {
      const store = mockStore();
      // mock post to reject request
      axios._initPost({error: 'fail'});
      return store.dispatch(asyncActions.postMove(
        {
          owner: 'player_1',
          room: 'room_1'
        },
        0
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_MOVE_REQUEST,
            player: 'player_1',
            position: 0
          },
          {type: types.POST_MOVE_FAILURE}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/room/room_1/move',
          {
            player: 'player_1',
            position: 0
          }
        );
        // expect error to be set on unsuccessful post
        expect(actions.setError).toHaveBeenCalledTimes(1);
        expect(actions.setError).toHaveBeenCalledWith(
          {error: 'fail'},
          types.POST_MOVE_FAILURE
        );
      });
    });
  });
  describe('postReset', () => {
    beforeEach(() => {
      actions._initMockResetGame({type: types.POST_RESET_SUCCESS});
      actions._initSetError({type: types.POST_RESET_FAILURE});
    });
    test('should reset game on successful post', () => {
      const store = mockStore();
      // mock post to resolve request
      axios._initPost({data: 'success'});
      return store.dispatch(asyncActions.postReset(
        {
          owner: 'player_1',
          room: 'room_1'
        }
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_RESET_REQUEST,
            player: 'player_1'
          },
          {type: types.POST_RESET_SUCCESS}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/room/room_1/reset',
          {
            player: 'player_1'
          }
        );
        // expect game to reset on successful post
        expect(actions.resetGame).toHaveBeenCalledTimes(1);
        expect(actions.resetGame).toHaveBeenCalledWith(
          'success',
          types.POST_RESET_SUCCESS
        );
      });
    });
    test('should set error on unsuccessful post', () => {
      const store = mockStore();
      // mock post to reject request
      axios._initPost({error: 'fail'});
      return store.dispatch(asyncActions.postReset(
        {
          owner: 'player_1',
          room: 'room_1'
        }
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_RESET_REQUEST,
            player: 'player_1'
          },
          {type: types.POST_RESET_FAILURE}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/room/room_1/reset',
          {
            player: 'player_1'
          }
        );
        // expect error to be set on unsuccessful post
        expect(actions.setError).toHaveBeenCalledTimes(1);
        expect(actions.setError).toHaveBeenCalledWith(
          {error: 'fail'},
          types.POST_RESET_FAILURE
        );
      });
    });
  });
  describe('postMessage', () => {
    beforeEach(() => {
      actions._initInsertMessage({type: types.POST_MESSAGE_SUCCESS});
      actions._initSetError({type: types.POST_MESSAGE_FAILURE});
    });
    test('should insert message on successful post', () => {
      const store = mockStore();
      // mock post to resolve request
      axios._initPost({data: 'pong'});
      return store.dispatch(asyncActions.postMessage(
        {
          owner: 'player_1',
          room: 'room_1'
        },
        'ping'
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_MESSAGE_REQUEST,
            player: 'player_1',
            text: 'ping'
          },
          {type: types.POST_MESSAGE_SUCCESS}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/room/room_1/message',
          {
            player: 'player_1',
            text: 'ping'
          }
        );
        // expect on successful post to insert message to store
        expect(actions.insertMessage).toHaveBeenCalledTimes(1);
        expect(actions.insertMessage).toHaveBeenCalledWith(
          'pong',
          types.POST_MESSAGE_SUCCESS
        );
      });
    });
    test('should should set error on unsuccessful post', () => {
      const store = mockStore();
      // mock post to reject request
      axios._initPost({error: 'ping failed'});
      return store.dispatch(asyncActions.postMessage(
        {
          owner: 'player_1',
          room: 'room_1'
        },
        'ping'
      )).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: types.POST_MESSAGE_REQUEST,
            player: 'player_1',
            text: 'ping'
          },
          {type: types.POST_MESSAGE_FAILURE}
        ]);
        expect(axios.post).toHaveBeenCalledTimes(1);
        expect(axios.post).toHaveBeenCalledWith(
          '/api/room/room_1/message',
          {
            player: 'player_1',
            text: 'ping'
          }
        );
        // expect unsuccessful post to set error
        expect(actions.setError).toHaveBeenCalledTimes(1);
        expect(actions.setError).toHaveBeenCalledWith(
          {error: 'ping failed'},
          types.POST_MESSAGE_FAILURE
        );
      });
    });
  });
});
