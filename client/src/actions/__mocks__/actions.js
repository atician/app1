
export const _initMockResetGame = action => {
  resetGame.action = action;
};
export const resetGame = jest.fn(() => resetGame.action);

export const _initMockPerformMove = action => {
  performMove.action = action;
};
export const performMove = jest.fn(() => performMove.action);

export const _initMockStartGame = action => {
  startGame.action = action;
};
export const startGame = jest.fn(() => startGame.action);

export const _initMockSetAvatar = action => {
  setAvatar.action = action;
};
export const setAvatar = jest.fn(() => setAvatar.action);

export const addPlayer = jest.fn((_, type) => ({type}));

export const _initInsertMessage = action => {
  insertMessage.action = action;
};
export const insertMessage = jest.fn(() => insertMessage.action);

export const _initSetError = action => {
  setError.action = action;
};
export const setError = jest.fn(() => setError.action);

export const _initSetPlayers = action => {
  setPlayers.action = action;
};
export const setPlayers = jest.fn(() => setPlayers.action);

export const _initSetRoom = action => {
  setRoom.action = action;
};
export const setRoom = jest.fn(() => setRoom.action);
