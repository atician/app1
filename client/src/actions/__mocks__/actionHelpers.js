
export const _initMockHandleMode = action => {
  handleMode.action = action;
};
export const handleMode = jest.fn(() => handleMode.action);

export const _initMockHandleJoin = action => {
  handleJoin.action = action;
};
export const handleJoin = jest.fn(() => handleJoin.action);

export const _initMockHandlePlayers = action => {
  handlePlayers.action = action;
};
export const handlePlayers = jest.fn(() => handlePlayers.action);

export const _initMockHandleAvatar = action => {
  handleAvatar.action = action;
};
export const handleAvatar = jest.fn(() => handleAvatar.action);

export const _initMockHandleStart = action => {
  handleStart.action = action;
};
export const handleStart = jest.fn(() => handleStart.action);

export const _initMockHandleMove = action => {
  handleMove.action = action;
};
export const handleMove = jest.fn(() => handleMove.action);

export const _initMockAddSocketListener = action => {
  addSocketListener.action = action;
};
export const addSocketListener = jest.fn(() => addSocketListener.action);

export const _initMockRemoveSocketListener = action => {
  removeSocketListener.action = action;
};
export const removeSocketListener = jest.fn(() => removeSocketListener.action);
