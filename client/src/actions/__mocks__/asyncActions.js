
export const _initMockPostMessage = action => {
  postMessage.action = action;
};
export const postMessage = jest.fn(() => postMessage.action);

export const _initMockPostReset = action => {
  postReset.action = action;
};
export const postReset = jest.fn(() => postReset.action);

export const _initMockPostMove = action => {
  postMove.action = action;
};
export const postMove = jest.fn(() => postMove.action);

export const _initMockPostStart = action => {
  postStart.action = action;
};
export const postStart = jest.fn(() => postStart.action);

export const _initMockPatchAvatar = action => {
  patchAvatar.action = action;
};
export const patchAvatar = jest.fn(() => patchAvatar.action);

export const _initMockGetPlayers = action => {
  getPlayers.action = action;
};
export const getPlayers = jest.fn(() => getPlayers.action);

export const _initMockPostPlayer = action => {
  postPlayer.action = action;
};
export const postPlayer = jest.fn(() => postPlayer.action);

export const _initMockJoinGame = action => {
  joinGame.action = action;
};
export const joinGame = jest.fn(() => joinGame.action);
