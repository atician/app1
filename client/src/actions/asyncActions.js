import axios from 'axios';
import io from 'socket.io-client';
import * as actions from './actions';
import * as types from '../constants/actionTypes';

export const connectSocket = () => {
  return new Promise((resolve, reject) => {
    const socket = io();
    socket.on('connect', () => {
      resolve(socket);
    });
    socket.on('connect_error', (error) => {
      reject(error);
    });
  });
};
export const joinGame = name => (dispatch, getState) => {
  dispatch({
    type: types.CONNECT_SOCKET_REQUEST
  });
  return connectSocket()
    .then(
      socket => dispatch({
        type: types.CONNECT_SOCKET_SUCCESS,
        socket: socket
      }),
      err => dispatch(actions.setError(err, types.CONNECT_SOCKET_FAILURE))
    )
    .then(() => dispatch(postPlayer(getState(), name)));
};
export const postPlayer = (state, name) => dispatch => {
  dispatch({
    type: types.POST_PLAYER_REQUEST,
    name
  });
  return axios.post(`/api/player/${state.owner}`, { name })
    .then(
      res => dispatch(actions.setRoom(res.data, types.POST_PLAYER_SUCCESS)),
      err => dispatch(actions.setError(err, types.POST_PLAYER_FAILURE))
    );
};
export const getPlayers = state => dispatch => {
  dispatch({
    type: types.GET_PLAYER_REQUEST
  });
  return axios.get(`/api/room/${state.room}/player/${state.owner}`)
    .then(
      res => dispatch(actions.setPlayers(res.data, types.GET_PLAYER_SUCCESS)),
      err => dispatch(actions.setError(err, types.GET_PLAYER_FAILURE))
    );
};
export const patchAvatar = ({ room, owner }, avatar) => dispatch => {
  const data = {
    avatar
  };
  dispatch({
    type: types.PATCH_AVATAR_REQUEST,
    owner,
    room,
    ...data
  });
  return axios.patch(`/api/room/${room}/player/${owner}/avatar`, data)
    .then(
      res => dispatch(actions.setAvatar(res.data, types.PATCH_AVATAR_SUCCESS)),
      err => dispatch(actions.setError(err, types.PATCH_AVATAR_FAILURE))
    );
};
export const postStart = state => dispatch => {
  const data = {
    player: state.owner
  };
  dispatch({
    type: types.POST_START_REQUEST,
    ...data
  });
  return axios.post(`/api/room/${state.room}/start`, data)
    .then(
      res => dispatch(actions.startGame(res.data, types.POST_START_SUCCESS, state)),
      err => dispatch(actions.setError(err, types.POST_START_FAILURE))
    );
};
export const postMove = (state, position) => dispatch => {
  const data = {
    player: state.owner,
    position
  };
  dispatch({
    type: types.POST_MOVE_REQUEST,
    ...data
  });
  return axios.post(`/api/room/${state.room}/move`, data)
    .then(
      res => dispatch(actions.performMove(res.data, types.POST_MOVE_SUCCESS, state)),
      err => dispatch(actions.setError(err, types.POST_MOVE_FAILURE))
    );
};
export const postReset = state => dispatch => {
  const data = {
    player: state.owner
  };
  dispatch({
    type: types.POST_RESET_REQUEST,
    ...data
  });
  return axios.post(`/api/room/${state.room}/reset`, data)
    .then(
      res => dispatch(actions.resetGame(res.data, types.POST_RESET_SUCCESS)),
      err => dispatch(actions.setError(err, types.POST_RESET_FAILURE))
    );
};
export const postMessage = (state, text) => dispatch => {
  const data = {
    player: state.owner,
    text
  };
  dispatch({
    type: types.POST_MESSAGE_REQUEST,
    ...data
  });
  return axios.post(`/api/room/${state.room}/message`, data)
    .then(
      res => dispatch(actions.insertMessage(res.data, types.POST_MESSAGE_SUCCESS)),
      err => dispatch(actions.setError(err, types.POST_MESSAGE_FAILURE))
    );
};
