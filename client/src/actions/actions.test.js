import * as actions from './actions';
import * as types from '../constants/actionTypes';
import * as gameConstants from '../constants/gameConstants';

// mock getWinner used in performMove action creator
import getWinner, { _setMockWinner } from '../utils/getWinner';
jest.mock('../utils/getWinner');

describe('actions', () => {
  test('setRoom should create an action to set room', () => {
    expect(actions.setRoom({
      room: 'room_1'
    }, types.POST_PLAYER_SUCCESS)).toEqual({
      type: types.POST_PLAYER_SUCCESS,
      room: 'room_1'
    });
  });
  test('addPlayer should create an action to add a single player', () => {
    expect(actions.addPlayer(
      {
        id: 'player_1',
        name: 'Joe',
        avatar: null,
        score: 0
      },
      types.ADD_PLAYER
    )).toEqual({
      type: types.ADD_PLAYER,
      player: {
        id: 'player_1',
        name: 'Joe',
        avatar: null,
        score: 0
      }
    });
  });
  test('setPlayers should create an action to set players', () => {
    const players = {
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 0
        },
        player_2: {
          id: 'player_2',
          name: 'Jane',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1', 'player_2']
    };
    expect(actions.setPlayers({players}, types.GET_PLAYER_SUCCESS)).toEqual({
      type: types.GET_PLAYER_SUCCESS,
      players
    });
  });
  test('setAvatar should create an action to set avatar', () => {
    expect(actions.setAvatar({
      player: 'player_1',
      avatar: 'CROSS'
    }, types.PATCH_AVATAR_SUCCESS)).toEqual({
      type: types.PATCH_AVATAR_SUCCESS,
      player: 'player_1',
      avatar: 'CROSS'
    });
    expect(actions.setAvatar({
      player: 'player_1',
      avatar: null
    }, types.PATCH_AVATAR_SUCCESS)).toEqual({
      type: types.PATCH_AVATAR_SUCCESS,
      player: 'player_1',
      avatar: null
    });
  });
  test('startGame should create an action to set currPlayer and moves', () => {
    expect(actions.startGame({
      maxMoves: gameConstants.MAX_MOVES
    }, types.POST_START_SUCCESS, {
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CIRCLE',
            score: 0
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      }
    })).toEqual({
      type: types.POST_START_SUCCESS,
      currPlayer: 'player_1',
      maxMoves: gameConstants.MAX_MOVES
    });
  });
  test('performMove should create an action to set currPlayer, winner, and moves', () => {
    _setMockWinner(null);
    expect(actions.performMove({
      position: 0
    }, types.POST_MOVE_SUCCESS, {
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CIRCLE',
            score: 0
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      },
      currPlayer: 'player_1',
      moves: Array(gameConstants.MAX_MOVES).fill(null)
    })).toEqual({
      type: types.POST_MOVE_SUCCESS,
      position: 0,
      player: 'player_1',
      currPlayer: 'player_2',
      winner: null
    });
    expect(getWinner).toHaveBeenCalledTimes(1);
    expect(getWinner).toHaveBeenCalledWith(0, 'player_1',
        Array(gameConstants.MAX_MOVES).fill(null));

    getWinner.mockClear();
    _setMockWinner('player_1');
    expect(actions.performMove({
      position: 8
    }, types.POST_MOVE_SUCCESS, {
      players: {
        byId: {
          player_1: {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CIRCLE',
            score: 0
          },
          player_2: {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 1
          }
        },
        ids: ['player_2', 'player_1']
      },
      currPlayer: 'player_2',
      moves: [
        null, null, null,
        'player_1', 'player_2', null,
        null, null, null
      ]
    })).toEqual({
      type: types.POST_MOVE_SUCCESS,
      position: 8,
      player: 'player_2',
      currPlayer: 'player_1',
      // test that the mock works even though 'player_1' didn't win
      winner: 'player_1'
    });
    expect(getWinner).toHaveBeenCalledTimes(1);
    expect(getWinner).toHaveBeenCalledWith(8, 'player_2', [
      null, null, null,
      'player_1', 'player_2', null,
      null, null, null
    ]);
  });
  test('resetGame should create an action to set players', () => {
    const players = {
      byId: {
        player_1: {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CIRCLE',
          score: 0
        },
        player_2: {
          id: 'player_2',
          name: 'Jane',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1', 'player_2']
    };
    expect(actions.resetGame({players}, types.POST_RESET_SUCCESS)).toEqual({
      type: types.POST_RESET_SUCCESS,
      players
    });
  });
  test('insertMessage should create an action to add message', () => {
    const message = {
      id: 'message_1',
      player: 'player_1',
      text: 'North'
    }
    expect(actions.insertMessage({message},
        types.POST_MESSAGE_SUCCESS)).toEqual({
      type: types.POST_MESSAGE_SUCCESS,
      message
    });
  });
  test('setError should create an action to set error', () => {
    expect(actions.setError({
      error: 'Bad'
    }, types.POST_MESSAGE_FAILURE)).toEqual({
      type: types.POST_MESSAGE_FAILURE,
      error: 'Bad'
    });
  })
});
