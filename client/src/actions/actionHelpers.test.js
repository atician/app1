import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionHelpers from './actionHelpers';
import * as types from '../constants/actionTypes';
import * as gameConstants from '../constants/gameConstants';

// mocked modules
import * as asyncActions from './asyncActions';
import * as actions from './actions';
import getNextMove, { _initMockGetNextMove } from '../utils/getNextMove';
jest.mock('./asyncActions');
jest.mock('./actions');
jest.mock('../utils/getNextMove');

const mockStore = configureStore([thunk]);

describe('action helpers', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  describe('handleMode', () => {
    expect(actionHelpers.handleMode(gameConstants.SINGLE_PLAYER)).toEqual({
      type: types.SET_MODE,
      mode: gameConstants.SINGLE_PLAYER
    });
  });
  describe('handleJoin', () => {
    test('should not dispatch because state is loading', () => {
      const store = mockStore({
        isLoading: true
      });
      store.dispatch(actionHelpers.handleJoin());
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because mode is not set', () => {
      const store = mockStore({
        isLoading: false,
        mode: null
      });
      store.dispatch(actionHelpers.handleJoin());
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because player\'s name is not provided', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.SINGLE_PLAYER
      });
      store.dispatch(actionHelpers.handleJoin());
      expect(store.getActions()).toEqual([]);
    });
    test('should dispatch for SINGLE_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.SINGLE_PLAYER
      });
      store.dispatch(actionHelpers.handleJoin('Joe'));
      // expect dispatch to add player, then dispatch to add PC player
      expect(store.getActions()).toEqual([
        {type: types.ADD_PLAYER},
        {type: types.ADD_PC_PLAYER}
      ]);
      expect(actions.addPlayer).toHaveBeenCalledTimes(2);
      expect(actions.addPlayer.mock.calls).toEqual([
        [
          {
            id: 'Joe',
            name: 'Joe',
            avatar: null,
            score: 0
          },
          types.ADD_PLAYER
        ], [
          gameConstants.PC_PLAYER,
          types.ADD_PC_PLAYER
        ]
      ]);
    });
    test('should dispatch for MULTI_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER
      });
      asyncActions._initMockJoinGame({type: types.POST_PLAYER_REQUEST});
      store.dispatch(actionHelpers.handleJoin('Joe'));
      expect(store.getActions()).toEqual([{type: types.POST_PLAYER_REQUEST}]);
      expect(asyncActions.joinGame).toHaveBeenCalledTimes(1);
      expect(asyncActions.joinGame).toHaveBeenCalledWith('Joe');
    });
  });
  describe('handlePlayers', () => {
    test('should not dispatch because state is loading', () => {
      const store = mockStore({
        isLoading: true
      });
      store.dispatch(actionHelpers.handlePlayers());
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because mode is not set', () => {
      const store = mockStore({
        isLoading: false,
        mode: null
      });
      store.dispatch(actionHelpers.handlePlayers());
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because mode is SINGLE_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.SINGLE_PLAYER
      });
      store.dispatch(actionHelpers.handlePlayers());
      expect(store.getActions()).toEqual([]);
    });
    test('should dispatch for MULTI_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER
      });
      asyncActions._initMockGetPlayers({type: types.GET_PLAYER_REQUEST});
      store.dispatch(actionHelpers.handlePlayers());
      expect(store.getActions()).toEqual([{type: types.GET_PLAYER_REQUEST}]);
      expect(asyncActions.getPlayers).toHaveBeenCalledTimes(1);
      expect(asyncActions.getPlayers).toHaveBeenCalledWith({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER
      });
    });
  });
  describe('handleAvatar', () => {
    test('should not dispatch because state is loading', () => {
      const store = mockStore({
        isLoading: true
      });
      store.dispatch(actionHelpers.handleAvatar());
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because mode is not selected', () => {
      const store = mockStore({
        isLoading: false,
        mode: null
      });
      store.dispatch(actionHelpers.handleAvatar());
      expect(store.getActions()).toEqual([]);
    });
    test('should dispatch for SINGLE_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.SINGLE_PLAYER,
        owner: 'player_1'
      });
      actions._initMockSetAvatar({type: types.EVENT_AVATAR});
      store.dispatch(actionHelpers.handleAvatar('CROSS'));
      expect(store.getActions()).toEqual([{type: types.EVENT_AVATAR}]);
      expect(actions.setAvatar).toHaveBeenCalledTimes(1);
      expect(actions.setAvatar).toHaveBeenCalledWith(
        {
          avatar: 'CROSS',
          player: 'player_1'
        },
        types.EVENT_AVATAR
      );
    });
    test('should dispatch for MULTI_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER
      });
      asyncActions._initMockPatchAvatar({type: types.PATCH_AVATAR_REQUEST});
      store.dispatch(actionHelpers.handleAvatar('CROSS'));
      expect(store.getActions()).toEqual([{type: types.PATCH_AVATAR_REQUEST}]);
      expect(asyncActions.patchAvatar).toHaveBeenCalledTimes(1);
      expect(asyncActions.patchAvatar).toHaveBeenCalledWith(
        {
          isLoading: false,
          mode: gameConstants.MULTI_PLAYER
        },
        'CROSS'
      );
    });
  });
  describe('handleStart', () => {
    test('should not dispatch because state is loading', () => {
      const store = mockStore({
        isLoading: true
      });
      store.dispatch(actionHelpers.handleStart());
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because mode is not selected', () => {
      const store = mockStore({
        isLoading: false,
        mode: null
      });
      store.dispatch(actionHelpers.handleStart());
      expect(store.getActions()).toEqual([]);
    });
    test('should dispatch for SINGLE_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.SINGLE_PLAYER
      });
      actions._initMockStartGame({type: types.EVENT_START});
      store.dispatch(actionHelpers.handleStart());
      expect(store.getActions()).toEqual([{type: types.EVENT_START}]);
      expect(actions.startGame).toHaveBeenCalledTimes(1);
      expect(actions.startGame).toHaveBeenCalledWith(
        {maxMoves: gameConstants.MAX_MOVES},
        types.EVENT_START,
        {
          isLoading: false,
          mode: gameConstants.SINGLE_PLAYER
        }
      );
    });
    test('should dispatch for MULTI_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER
      });
      asyncActions._initMockPostStart({type: types.POST_START_REQUEST});
      store.dispatch(actionHelpers.handleStart());
      expect(store.getActions()).toEqual([{type: types.POST_START_REQUEST}]);
      expect(asyncActions.postStart).toHaveBeenCalledTimes(1);
      expect(asyncActions.postStart).toHaveBeenCalledWith({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER
      });
    });
  });
  describe('handlePCMove', () => {
    describe('should not dispatch because', () => {
      test('not PC\'s turn', () => {
        const store = mockStore({
          currPlayer: 'player_1'
        });
        store.dispatch(actionHelpers.handlePCMove());
        expect(store.getActions()).toEqual([]);
      });
      test('there is a winner', () => {
        const store = mockStore({
          currPlayer: 'PC_PLAYER',
          winner: 'player_1'
        });
        store.dispatch(actionHelpers.handlePCMove());
        expect(store.getActions()).toEqual([]);
      });
      test('game is a tie', () => {
        const store = mockStore({
          currPlayer: 'PC_PLAYER',
          winner: null,
          moves: Array(gameConstants.MAX_MOVES).fill('player_1')
        });
        store.dispatch(actionHelpers.handlePCMove());
        expect(store.getActions()).toEqual([]);
      });
    });
    test('should dispatch PC\' move', () => {
      const store = mockStore({
        owner: 'player_1',
        currPlayer: 'PC_PLAYER',
        winner: null,
        moves: Array(gameConstants.MAX_MOVES).fill(null)
      });
      jest.useFakeTimers();
      actions._initMockPerformMove({type: types.EVENT_MOVE});
      _initMockGetNextMove(0);
      store.dispatch(actionHelpers.handlePCMove());
      // PC should not have performed move
      expect(store.getActions()).toEqual([]);
      expect(actions.performMove).not.toHaveBeenCalled();
      expect(getNextMove).not.toHaveBeenCalled();
      // advance timer to perform PC's move
      jest.runTimersToTime(gameConstants.PC_MOVE_DELAY);
      expect(store.getActions()).toEqual([{type: types.EVENT_MOVE}]);
      expect(actions.performMove).toHaveBeenCalledTimes(1);
      expect(actions.performMove).toHaveBeenCalledWith(
        {position: 0},
        types.EVENT_MOVE,
        {
          owner: 'player_1',
          currPlayer: 'PC_PLAYER',
          winner: null,
          moves: Array(gameConstants.MAX_MOVES).fill(null)
        }
      );
      expect(getNextMove).toHaveBeenCalledTimes(1);
      expect(getNextMove).toHaveBeenCalledWith(
        gameConstants.PC_PLAYER.id,
        'player_1',
        Array(gameConstants.MAX_MOVES).fill(null)
      );
    });
  });
  describe('handleMove', () => {
    describe('should not dispatch because', () => {
      test('state is loading', () => {
        const store = mockStore({
          isLoading: true
        });
        store.dispatch(actionHelpers.handleMove());
        expect(store.getActions()).toEqual([]);
      });
      test('not your turn', () => {
        const store = mockStore({
          isLoading: false,
          owner: 'player_1',
          currPlayer: 'player_2'
        });
        store.dispatch(actionHelpers.handleMove());
        expect(store.getActions()).toEqual([]);
      });
      test('there is a winner', () => {
        const store = mockStore({
          isLoading: false,
          owner: 'player_1',
          currPlayer: 'player_1',
          winner: 'player_1'
        });
        store.dispatch(actionHelpers.handleMove());
        expect(store.getActions()).toEqual([]);
      });
      test('invalid position', () => {
        const store = mockStore({
          isLoading: false,
          owner: 'player_1',
          currPlayer: 'player_1',
          winner: null,
          moves: [
            null, null, 'player_2',
            null, null, null,
            null, null, null
          ]
        });
        store.dispatch(actionHelpers.handleMove());
        expect(store.getActions()).toEqual([]);
        store.dispatch(actionHelpers.handleMove(-1));
        expect(store.getActions()).toEqual([]);
      });
      test('position on board is already taken', () => {
        const store = mockStore({
          isLoading: false,
          owner: 'player_1',
          currPlayer: 'player_1',
          winner: null,
          moves: [
            null, null, 'player_2',
            null, null, null,
            null, null, null
          ]
        });
        store.dispatch(actionHelpers.handleMove(2));
        expect(store.getActions()).toEqual([]);
      });
      test('mode is not selected', () => {
        const store = mockStore({
          isLoading: false,
          owner: 'player_1',
          currPlayer: 'player_1',
          winner: null,
          moves: [
            null, null, 'player_2',
            null, null, null,
            null, null, null
          ]
        });
        store.dispatch(actionHelpers.handleMove(0));
        expect(store.getActions()).toEqual([]);
      });
    });
    test('should dispatch for SINGLE_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.SINGLE_PLAYER,
        owner: 'player_1',
        currPlayer: 'player_1',
        winner: null,
        moves: [
          null, null, 'PC_PLAYER',
          null, null, null,
          null, null, null
        ]
      });
      actions._initMockPerformMove({type: types.EVENT_MOVE});
      store.dispatch(actionHelpers.handleMove(0));
      // only test first dispatch
      // functions called on subsequent dispatches will be tested separately
      expect(store.getActions()[0]).toEqual({type: types.EVENT_MOVE});
      expect(actions.performMove).toHaveBeenCalledTimes(1);
      expect(actions.performMove).toHaveBeenCalledWith(
        {position: 0},
        types.EVENT_MOVE,
        {
          isLoading: false,
          mode: gameConstants.SINGLE_PLAYER,
          owner: 'player_1',
          currPlayer: 'player_1',
          winner: null,
          moves: [
            null, null, 'PC_PLAYER',
            null, null, null,
            null, null, null
          ]
        }
      );
    });
    test('should dispatch for MULTI_PLAYER', () => {
      const store = mockStore({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER,
        owner: 'player_1',
        currPlayer: 'player_1',
        winner: null,
        moves: [
          null, null, 'player_2',
          null, null, null,
          null, null, null
        ]
      });
      asyncActions._initMockPostMove({type: types.POST_MOVE_REQUEST});
      store.dispatch(actionHelpers.handleMove(5));
      // only test first dispatch
      // functions called on subsequent dispatches will be tested separately
      expect(store.getActions()[0]).toEqual({type: types.POST_MOVE_REQUEST});
      expect(asyncActions.postMove).toHaveBeenCalledTimes(1);
      expect(asyncActions.postMove).toHaveBeenCalledWith(
        {
          isLoading: false,
          mode: gameConstants.MULTI_PLAYER,
          owner: 'player_1',
          currPlayer: 'player_1',
          winner: null,
          moves: [
            null, null, 'player_2',
            null, null, null,
            null, null, null
          ]
        },
        5
      );
    });
  });
  describe('handleReset', () => {
    test('should not dispatch because state is loading', () => {
      const store = mockStore({isLoading: true});
      store.dispatch(actionHelpers.handleReset());
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because mode is not set', () => {
      const store = mockStore({
        isLoading: false,
        mode: null
      });
      store.dispatch(actionHelpers.handleReset());
      expect(store.getActions()).toEqual([]);
    });
    describe('should dispatch for SINGLE_PLAYER', () => {
      test('win', () => {
        let store = mockStore({
          isLoading: false,
          mode: gameConstants.SINGLE_PLAYER,
          players: {
            byId: {
              player_1: {
                id: 'player_1',
                name: 'Joe',
                avatar: 'CIRCLE',
                score: 0
              },
              PC_PLAYER: {
                id: 'PC_PLAYER',
                name: 'PC',
                avatar: 'CROSS',
                score: 0
              }
            },
            ids: ['PC_PLAYER', 'player_1']
          },
          winner: 'player_1',
          owner: 'player_1',
          moves: Array(gameConstants.MAX_MOVES).fill(null)
        });
        actions._initMockResetGame({type: types.EVENT_RESET});
        store.dispatch(actionHelpers.handleReset());
        // only test first dispatch
        // functions called on subsequent dispatches will be tested separately
        expect(store.getActions()[0]).toEqual({type: types.EVENT_RESET});
        expect(actions.resetGame).toHaveBeenCalledTimes(1);
        expect(actions.resetGame).toHaveBeenCalledWith({
          players: {
            byId: {
              player_1: {
                id: 'player_1',
                name: 'Joe',
                avatar: 'CIRCLE',
                // expect the score to be incremented
                score: 1
              },
              PC_PLAYER: {
                id: 'PC_PLAYER',
                name: 'PC',
                avatar: 'CROSS',
                score: 0
              }
            },
            // expect the order of players to be changed
            ids: ['player_1', 'PC_PLAYER']
          }
        }, types.EVENT_RESET);
      });
      test('lose', () => {
        let store = mockStore({
          isLoading: false,
          mode: gameConstants.SINGLE_PLAYER,
          players: {
            byId: {
              player_1: {
                id: 'player_1',
                name: 'Joe',
                avatar: 'CIRCLE',
                score: 0
              },
              PC_PLAYER: {
                id: 'PC_PLAYER',
                name: 'PC',
                avatar: 'CROSS',
                score: 0
              }
            },
            ids: ['player_1', 'PC_PLAYER']
          },
          currPlayer: 'PC_PLAYER',
          winner: 'PC_PLAYER',
          owner: 'player_1',
          moves: Array(gameConstants.MAX_MOVES).fill(null)
        });
        actions._initMockResetGame({type: types.EVENT_RESET});
        store.dispatch(actionHelpers.handleReset());
        // only test first dispatch
        // functions called on subsequent dispatches will be tested separately
        expect(store.getActions()[0]).toEqual({type: types.EVENT_RESET});
        expect(actions.resetGame).toHaveBeenCalledTimes(1);
        expect(actions.resetGame).toHaveBeenCalledWith({
          players: {
            byId: {
              player_1: {
                id: 'player_1',
                name: 'Joe',
                avatar: 'CIRCLE',
                score: 0
              },
              PC_PLAYER: {
                id: 'PC_PLAYER',
                name: 'PC',
                avatar: 'CROSS',
                // expect the score to be incremented
                score: 1
              }
            },
            // expect the order of players to be changed
            ids: ['PC_PLAYER', 'player_1']
          }
        }, types.EVENT_RESET);
      });
      test('tie', () => {
        let store = mockStore({
          isLoading: false,
          mode: gameConstants.SINGLE_PLAYER,
          players: {
            byId: {
              player_1: {
                id: 'player_1',
                name: 'Joe',
                avatar: 'CIRCLE',
                score: 0
              },
              PC_PLAYER: {
                id: 'PC_PLAYER',
                name: 'PC',
                avatar: 'CROSS',
                score: 0
              }
            },
            ids: ['player_1', 'PC_PLAYER']
          },
          winner: null,
          owner: 'player_1',
          moves: Array(gameConstants.MAX_MOVES).fill(null)
        });
        actions._initMockResetGame({type: types.EVENT_RESET});
        store.dispatch(actionHelpers.handleReset());
        // only test first dispatch
        // functions called on subsequent dispatches will be tested separately
        expect(store.getActions()[0]).toEqual({type: types.EVENT_RESET});
        expect(actions.resetGame).toHaveBeenCalledTimes(1);
        expect(actions.resetGame).toHaveBeenCalledWith({
          players: {
            byId: {
              player_1: {
                id: 'player_1',
                name: 'Joe',
                avatar: 'CIRCLE',
                score: 0
              },
              PC_PLAYER: {
                id: 'PC_PLAYER',
                name: 'PC',
                avatar: 'CROSS',
                score: 0
              }
            },
            ids: ['player_1', 'PC_PLAYER']
          }
        }, types.EVENT_RESET);
      });
    });
    test('expect dispatch for MULTI_PLAYER', () => {
      let store = mockStore({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER
      });
      asyncActions._initMockPostReset({type: types.POST_RESET_SUCCESS});
      store.dispatch(actionHelpers.handleReset());
      expect(store.getActions()).toEqual([{type: types.POST_RESET_SUCCESS}]);
      expect(asyncActions.postReset).toHaveBeenCalledTimes(1);
      expect(asyncActions.postReset).toHaveBeenCalledWith({
        isLoading: false,
        mode: gameConstants.MULTI_PLAYER
      });
    });
  });
  describe('handleMessage', () => {
    test('should not dispatch because there is no text', () => {
      const store = mockStore({});
      store.dispatch(actionHelpers.handleMessage());
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because state is loading', () => {
      const store = mockStore({isLoading: true});
      store.dispatch(actionHelpers.handleMessage('Hey'));
      expect(store.getActions()).toEqual([]);
    });
    test('should not dispatch because mode is not MULTI_PLAYER', () => {
      let store = mockStore({
        mode: null,
        isLoading: false
      });
      store.dispatch(actionHelpers.handleMessage('Hey'));
      expect(store.getActions()).toEqual([]);

      store = mockStore({
        mode: gameConstants.SINGLE_PLAYER,
        isLoading: false
      });
      store.dispatch(actionHelpers.handleMessage('Hey'));
      expect(store.getActions()).toEqual([]);
    });
    test('should dispatch postMessage', () => {
      asyncActions._initMockPostMessage({
        type: types.POST_MESSAGE_REQUEST
      });
      const store = mockStore({
        mode: gameConstants.MULTI_PLAYER,
        isLoading: false
      });
      store.dispatch(actionHelpers.handleMessage('Hey'));
      expect(store.getActions()).toEqual([{type: types.POST_MESSAGE_REQUEST}]);
      expect(asyncActions.postMessage).toHaveBeenCalledTimes(1);
      expect(asyncActions.postMessage).toHaveBeenCalledWith({
        mode: gameConstants.MULTI_PLAYER,
        isLoading: false
      }, 'Hey');
    });
  });
});
