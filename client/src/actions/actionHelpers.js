import * as actions from './actions';
import * as asyncActions from './asyncActions';
import * as types from '../constants/actionTypes';
import getNextMove from '../utils/getNextMove';
import {
  SINGLE_PLAYER, MULTI_PLAYER, MAX_MOVES, PC_PLAYER, PC_MOVE_DELAY
} from '../constants/gameConstants';

export const addSocketListener = (event, handler) => (dispatch, getState) => {
  const state = getState();
  if (state.mode === MULTI_PLAYER) {
    state.socket.on(event, data => dispatch(handler(data, event, getState())));
  }
};
export const removeSocketListener = event => (dispatch, getState) => {
  const { socket, mode } = getState();
  if (mode === MULTI_PLAYER) {
    socket.off(event);
  }
};
export const handleMode = mode => ({
  type: types.SET_MODE,
  mode
});
export const handleJoin = name => (dispatch, getState) => {
  const { mode, isLoading } = getState();
  if (isLoading) {
    return;
  }
  if (!name || !mode) {
    return;
  }
  switch (mode) {
    case SINGLE_PLAYER:
      dispatch(actions.addPlayer(
        {
          id: name,
          name,
          avatar: null,
          score: 0
        },
        types.ADD_PLAYER
      ));
      dispatch(actions.addPlayer(
        PC_PLAYER,
        types.ADD_PC_PLAYER
      ));
      break;
    case MULTI_PLAYER:
      dispatch(asyncActions.joinGame(name));
      break;
    default:
      return;
  }
};
export const handlePlayers = () => (dispatch, getState) => {
  const state = getState();
  if (state.isLoading) {
    return;
  }
  switch (state.mode) {
    case MULTI_PLAYER:
      dispatch(asyncActions.getPlayers(state));
      break;
    default:
      break;
  }
};
export const handleAvatar = avatar => (dispatch, getState) => {
  const state = getState();
  if (state.isLoading) {
    return;
  }
  switch (state.mode) {
    case SINGLE_PLAYER:
      dispatch(actions.setAvatar({avatar, player: state.owner}, types.EVENT_AVATAR));
      break;
    case MULTI_PLAYER:
      dispatch(asyncActions.patchAvatar(state, avatar));
      break;
    default:
      break;
  }
};
export const handleStart = () => (dispatch, getState) => {
  const state = getState();
  if (state.isLoading) {
    return;
  }
  switch (state.mode) {
    case SINGLE_PLAYER:
      dispatch(actions.startGame({maxMoves: MAX_MOVES}, types.EVENT_START, state));
      break;
    case MULTI_PLAYER:
      dispatch(asyncActions.postStart(state));
      break;
    default:
      break;
  }
};
export const handlePCMove = () => (dispatch, getState) => {
  const state = getState();
  if (state.currPlayer !== PC_PLAYER.id
      || state.winner
      || !state.moves.filter(move => !move).length) {
    return;
  }
  setTimeout(() => dispatch(
      actions.performMove(
        {position: getNextMove(PC_PLAYER.id, state.owner, state.moves)},
        types.EVENT_MOVE,
        state
      )), PC_MOVE_DELAY);
};
export const handleMove = position => (dispatch, getState) => {
  const state = getState();
  if (state.isLoading) {
    return;
  }
  if (state.currPlayer !== state.owner
      || state.winner
      || state.moves[position]) {
    return;
  }
  switch (state.mode) {
    case SINGLE_PLAYER:
      dispatch(actions.performMove({position}, types.EVENT_MOVE, state));
      dispatch(handlePCMove());
      break;
    case MULTI_PLAYER:
      dispatch(asyncActions.postMove(state, position));
      break;
    default:
      break;
  }
};
export const handleReset = () => (dispatch, getState) => {
  const state = getState();
  if (state.isLoading) {
    return;
  }
  switch (state.mode) {
    case SINGLE_PLAYER:
      let players;
      if (state.winner) {
        players = {
          byId: {...state.players.byId},
          ids: [...state.players.ids]
        };
        ++players.byId[state.winner].score;
        players.ids.sort((a, b) => players.byId[b].score - players.byId[a].score);
      } else {
        players = state.players;
      }
      dispatch(actions.resetGame({players}, types.EVENT_RESET));
      dispatch(handlePCMove());
      break;
    case MULTI_PLAYER:
      dispatch(asyncActions.postReset(state));
      break;
    default:
      break;
  }
};
export const handleMessage = text => (dispatch, getState) => {
  if (!text) {
    return;
  }
  const state = getState();
  if (state.isLoading) {
    return;
  }
  switch (state.mode) {
    case MULTI_PLAYER:
      dispatch(asyncActions.postMessage(state, text));
      break;
    default:
      break;
  }
};
