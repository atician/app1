import getWinner from '../utils/getWinner';

export const setRoom = ({ room }, type) => ({
  type,
  room
});
export const addPlayer = (player, type) => ({
  type,
  player
});
export const setPlayers = ({ players }, type) => ({
  type,
  players
});
export const setAvatar = ({ player, avatar }, type) => ({
  type,
  player,
  avatar
});
export const startGame = ({ maxMoves }, type, state) => ({
  type,
  // set the current player to first player on leader board on game start
  currPlayer: state.players.ids[0],
  maxMoves
});
export const performMove = ({ position }, type, state) => {
  // get the currPlayer's index in players.ids
  // the next player is determined by incrementing this index
  const currPlayerIndex = state.players.ids.indexOf(state.currPlayer);
  return {
    type,
    position,
    player: state.currPlayer,
    currPlayer: state.players.ids[(currPlayerIndex + 1) % state.players.ids.length],
    winner: getWinner(position, state.currPlayer, state.moves)
  };
};
export const resetGame = ({ players }, type) => ({
  type,
  players
});
export const insertMessage = ({ message }, type) => ({
  type,
  message
});
export const setError = ({ error }, type) => ({
  type,
  error
});
