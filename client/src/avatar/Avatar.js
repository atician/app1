import React from 'react';
import styled from 'styled-components';
import { string, bool } from 'prop-types';

import Icon from '../icon/Icon';
import { FLEX, BACKGROUND_COLOR, SIZE } from '../constants';

const Avatar = ({ name, sm, md, lg, ...props }) => (
  <Wrapper round sm={sm} md={md} lg={lg} {...props}>
    <Icon name={name}/>
  </Wrapper>
);
Avatar.propTypes = {
  name: string,
  sm: bool,
  md: bool,
  lg: bool
};
const Wrapper = styled.div`
  ${FLEX};
  ${BACKGROUND_COLOR};
  ${SIZE};
  border-radius: 100px;
`;

export default Avatar;
