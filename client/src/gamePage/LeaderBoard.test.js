import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import LeaderBoard, { Header } from './LeaderBoard';
import PlayerDetails from '../utils/PlayerDetails';

Enzyme.configure({adapter: new Adapter()});

let props, wrapper;

describe('LeaderBoard', () => {
  beforeEach(() => {
    props = {
      players: {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'JOE',
            avatar: 'CIRCLE',
            score: 0
          },
          'player_2': {
            id: 'player_2',
            name: 'JANE',
            avatar: 'CROSS',
            score: 1
          }
        },
        ids: ['player_2', 'player_1']
      },
      owner: 'player_1',
      currPlayer: 'player_1'
    };
    wrapper = shallow(<LeaderBoard {...props}/>);
  });
  test('should render component', () => {
    expect(wrapper.find(Header).props().children).toBe('LEADER BOARD');
    expect(wrapper.find(PlayerDetails).length).toBe(2);
    // player 2 because player details are listed in order of props.players.ids
    expect(wrapper.find(PlayerDetails).first().props()).toEqual({
      sm: true,
      active: false,
      id: 'player_2',
      avatar: 'CROSS',
      score: 1,
      name: 'JANE'
    });
    // player 1
    expect(wrapper.find(PlayerDetails).last().props()).toEqual({
      sm: true,
      active: true,
      id: 'player_1',
      avatar: 'CIRCLE',
      score: 0,
      name: 'JOE (ME)'
    });
  });
});
