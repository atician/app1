import React, { Component } from 'react';
import { connect } from 'react-redux';

import GamePage from './GamePage';
import { setPlayers } from '../actions/actions';
import {
  handlePlayers,
  addSocketListener,
  removeSocketListener
} from '../actions/actionHelpers';
import { MULTI_PLAYER } from '../constants/gameConstants';
import { EVENT_PLAYER } from '../constants/actionTypes';

export class GamePageContainer extends Component {
  componentDidMount() {
    this.props.dispatch(handlePlayers());
    this.props.dispatch(addSocketListener(EVENT_PLAYER, setPlayers));
  }
  componentWillUnmount() {
    this.props.dispatch(removeSocketListener(EVENT_PLAYER));
  }
  render() {
    return (
      <GamePage {...this.props}/>
    );
  }
}
GamePageContainer.propTypes = {
  ...GamePage.propTypes
};
export const mapStateToProps = ({ moves, players, currPlayer, owner, mode }) => ({
  players,
  currPlayer,
  owner,
  hasGameStarted: moves !== null,
  showMessenger: mode === MULTI_PLAYER
});
export default connect(
  mapStateToProps
)(GamePageContainer);
