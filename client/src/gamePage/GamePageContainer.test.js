import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { GamePageContainer, mapStateToProps } from './GamePageContainer';
import GamePage from './GamePage';
import * as gameConstants from '../constants/gameConstants';

jest.mock('../actions/actionHelpers');
jest.mock('../actions/actions');
import * as actionHelpers from '../actions/actionHelpers';
import * as actions from '../actions/actions';
import * as actionTypes from '../constants/actionTypes';

Enzyme.configure({adapter: new Adapter()});

let props, wrapper;

describe('GamePageContainer', () => {
  beforeEach(() => {
    actionHelpers._initMockHandlePlayers({type: null});
    actionHelpers._initMockAddSocketListener({type: null});
    actions._initSetPlayers({type: null});
    props = {
      players: {
        byId: {},
        ids: []
      },
      owner: 'player_1',
      currPlayer: null,
      hasGameStarted: false,
      showMessenger: false,
      dispatch: jest.fn()
    };
    wrapper = shallow(<GamePageContainer {...props}/>);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('should render component', () => {
    expect(wrapper.find(GamePage).exists()).toBe(true);
    expect(wrapper.find(GamePage).props()).toEqual(props);
    // componentDidMount
    expect(props.dispatch).toHaveBeenCalledTimes(2);
    expect(actionHelpers.handlePlayers).toHaveBeenCalledTimes(1);
    expect(actionHelpers.handlePlayers).toHaveBeenCalledWith();
    expect(actionHelpers.addSocketListener).toHaveBeenCalledTimes(1);
    expect(actionHelpers.addSocketListener).toHaveBeenCalledWith(
      actionTypes.EVENT_PLAYER,
      actions.setPlayers
    );
    props.dispatch.mockClear();
    // componentWillUnmount
    wrapper.unmount();
    expect(props.dispatch).toHaveBeenCalledTimes(1);
    expect(actionHelpers.removeSocketListener).toHaveBeenCalledTimes(1);
    expect(actionHelpers.removeSocketListener).toHaveBeenCalledWith(
      actionTypes.EVENT_PLAYER
    );
  });
  describe('mapStateToProps', () => {
    test('should start game if moves has been initialized', () => {
      // moves not initialized
      expect(mapStateToProps({
        players: props.players,
        currPlayer: props.currPlayer,
        owner: props.owner,
        moves: null,
        mode: gameConstants.SINGLE_PLAYER
      })).toEqual({
        players: props.players,
        currPlayer: props.currPlayer,
        owner: props.owner,
        hasGameStarted: false,
        showMessenger: false
      });
      // moves initialized
      expect(mapStateToProps({
        players: props.players,
        currPlayer: props.currPlayer,
        owner: props.owner,
        moves: Array(gameConstants.MAX_MOVES).fill(null),
        mode: gameConstants.SINGLE_PLAYER
      })).toEqual({
        players: props.players,
        currPlayer: props.currPlayer,
        owner: props.owner,
        hasGameStarted: true,
        showMessenger: false
      });
    });
    test('should show messenger for multi player mode', () => {
      expect(mapStateToProps({
        players: props.players,
        currPlayer: props.currPlayer,
        owner: props.owner,
        moves: null,
        mode: gameConstants.SINGLE_PLAYER
      })).toEqual({
        players: props.players,
        currPlayer: props.currPlayer,
        owner: props.owner,
        hasGameStarted: false,
        showMessenger: false
      });
      expect(mapStateToProps({
        players: props.players,
        currPlayer: props.currPlayer,
        owner: props.owner,
        moves: null,
        mode: gameConstants.MULTI_PLAYER
      })).toEqual({
        players: props.players,
        currPlayer: props.currPlayer,
        owner: props.owner,
        hasGameStarted: false,
        showMessenger: true
      });
    });
  });
});
