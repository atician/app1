import React from 'react';
import styled from 'styled-components';
import { string } from 'prop-types';

import PlayerDetails from '../utils/PlayerDetails';
import { playersType } from '../constants/propTypes';

const LeaderBoard = ({
  players,
  currPlayer,
  owner
}) => (
  <LeaderBoardWrapper>
    <Header>LEADER BOARD</Header>
    {players.ids.map(id => (
      <PlayerDetails
          sm
          key={id}
          active={id === currPlayer}
          {...players.byId[id]}
          name={id === owner ? players.byId[id].name + ' (ME)' : players.byId[id].name}/>
    ))}
  </LeaderBoardWrapper>
);
LeaderBoard.propTypes = {
  players: playersType.isRequired,
  currPlayer: string,
  owner: string.isRequired
};
const LeaderBoardWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-around;
  align-items: center;
  background-color: white;
  border-radius: 5px;
  padding 10px;
  ${'' /* width: 25%; */}
  height: 30%;
`;
export const Header = styled.span`
  font-size: 1.5em;
`;
export default LeaderBoard;
