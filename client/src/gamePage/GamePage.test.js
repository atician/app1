import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import GamePage from './GamePage';
import AvatarChooserContainer from '../avatarChooser/AvatarChooserContainer';
import BoardContainer from '../board/BoardContainer';
import NewGameModalContainer from '../modal/NewGameModalContainer';
import MessengerContainer from '../messenger/MessengerContainer';
import LeaderBoard from './LeaderBoard';

Enzyme.configure({adapter: new Adapter()});

let props, wrapper;

describe('GamePage', () => {
  beforeEach(() => {
    props = {
      players: {
        byId: {},
        ids: []
      },
      owner: 'player_1',
      currPlayer: null,
      hasGameStarted: false,
      showMessenger: false
    };
    wrapper = shallow(<GamePage {...props}/>);
  });
  test('should render GamePage', () => {
    // should render leader board and avatar chooser before game start
    expect(wrapper.find(AvatarChooserContainer).exists()).toBe(true);
    expect(wrapper.find(BoardContainer).exists()).toBe(false);
    expect(wrapper.find(LeaderBoard).exists()).toBe(true);
    expect(wrapper.find(MessengerContainer).exists()).toBe(false);
    expect(wrapper.find(NewGameModalContainer).exists()).toBe(false);
    // should render leader board, game board and modal after game start
    // should also render messenger for multi player mode
    wrapper.setProps({
      hasGameStarted: true,
      showMessenger: true
    });
    expect(wrapper.find(AvatarChooserContainer).exists()).toBe(false);
    expect(wrapper.find(BoardContainer).exists()).toBe(true);
    expect(wrapper.find(LeaderBoard).exists()).toBe(true);
    expect(wrapper.find(MessengerContainer).exists()).toBe(true);
    expect(wrapper.find(NewGameModalContainer).exists()).toBe(true);
  });
  test('should pass props to LeaderBoard', () => {
    expect(wrapper.find(LeaderBoard).props()).toEqual({
      players: props.players,
      owner: props.owner,
      currPlayer: null
    });
    const newProps = {
      players: {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: null,
            score: 0
          }
        },
        ids: ['player_1']
      },
      currPlayer: 'player_1',
      owner: 'player_1'
    };
    wrapper.setProps(newProps);
    expect(wrapper.find(LeaderBoard).props()).toEqual(newProps);
  });
});
