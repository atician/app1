import React from 'react';
import styled from 'styled-components';
import { bool, string } from 'prop-types';

import AvatarChooserContainer from '../avatarChooser/AvatarChooserContainer';
import BoardContainer from '../board/BoardContainer';
import NewGameModalContainer from '../modal/NewGameModalContainer';
import MessengerContainer from '../messenger/MessengerContainer';
import LeaderBoard from './LeaderBoard';
import { playersType } from '../constants/propTypes';

const GamePage = ({
  players,
  currPlayer,
  owner,
  hasGameStarted,
  showMessenger
}) => (
  <GamePageWrapper>
    <Game>
      <LeaderBoard players={players} currPlayer={currPlayer} owner={owner}/>
      {hasGameStarted ? <BoardContainer/> : <AvatarChooserContainer/>}
    </Game>
    {showMessenger && <MessengerContainer/>}
    {hasGameStarted && <NewGameModalContainer/>}
  </GamePageWrapper>
);
GamePage.propTypes = {
  players: playersType.isRequired,
  currPlayer: string,
  owner: string.isRequired,
  hasGameStarted: bool.isRequired,
  showMessenger: bool.isRequired
};
const GamePageWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  height: 100%;
`;
const Game = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  align-items: center;
  width: 100%;
  flex: 3;
  @media (min-width: 600px) {
    flex-flow: row-reverse nowrap;
  }
`;
export default GamePage;
