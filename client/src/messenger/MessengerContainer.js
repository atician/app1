import React, { Component } from 'react';
import { connect } from 'react-redux';
import { func } from 'prop-types';

import Messenger from './Messenger';
import Content from './Content';
import Status from './Status';
import Controls from './Controls';
import { insertMessage } from '../actions/actions'
import { handleMessage, addSocketListener } from '../actions/actionHelpers';
import { EVENT_MESSAGE } from '../constants/actionTypes';
import { MAX_MESSAGE_LENGTH } from '../constants/gameConstants';
import { playersType, messagesType } from '../constants/propTypes';

export class MessengerContainer extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.newMessage = null;
    this.state = {
      input: ''
    };
  }
  componentDidMount() {
    this.props.dispatch(addSocketListener(EVENT_MESSAGE, insertMessage));
  }
  componentDidUpdate(prevProps) {
    if (prevProps.messages.ids.length !== this.props.messages.ids.length) {
      this.newMessage.scrollIntoView();
    }
  }
  handleChange(e) {
    const input = e.target.value;
    if (input.length > MAX_MESSAGE_LENGTH) {
      return;
    }
    this.setState({ input });
  }
  handleSubmit(e) {
    if (e.key === 'Enter') {
      this.props.handleMessage(this.state.input);
      this.setState({ input: '' });
    }
  }
  render() {
    return (
      <Messenger>
        <Content
          players={this.props.players}
          messages={this.props.messages}
          newMessage={<li ref={node => this.newMessage = node}/>}/>
        <Status/>
        <Controls
          input={this.state.input}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}/>
      </Messenger>
    );
  }
}
MessengerContainer.propTypes = {
  players: playersType.isRequired,
  messages: messagesType.isRequired,
  handleMessage: func.isRequired
};
const mapStateToProps = ({ players, messages }) => ({
  players,
  messages
});
const mapDispatchToProps = dispatch => ({
  dispatch,
  handleMessage: message => dispatch(handleMessage(message.trim()))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessengerContainer);
