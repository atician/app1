import React from 'react';
import styled from 'styled-components';
import { node } from 'prop-types';

const Status = ({ children }) => (
  <StatusWrapper>
    {children}
  </StatusWrapper>
);
Status.propTypes = {
  children: node
};
const StatusWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  flex: 10;
  ${'' /* background-color: red; */}
`;

export default Status;
