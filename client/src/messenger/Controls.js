import React from 'react';
import styled from 'styled-components';
import { string, func } from 'prop-types';

import Input from '../utils/Input';

const Controls = ({ input, handleChange, handleSubmit }) => (
  <ControlsWrapper>
    <Input
      type="text"
      value={input}
      placeholder="MESSAGE"
      onChange={handleChange}
      onKeyDown={handleSubmit}
    />
  </ControlsWrapper>
);
Controls.propTypes = {
  input: string.isRequired,
  handleChange: func.isRequired,
  handleSubmit: func.isRequired
};
const ControlsWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  flex: 25;
  ${'' /* background-color: green; */}
  input {
    flex: 1;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    box-shadow: none;
    transition: none;
  }
`;

export default Controls;
