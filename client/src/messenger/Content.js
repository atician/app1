import React from 'react';
import styled from 'styled-components';
import { node } from 'prop-types';

import { playersType, messagesType } from '../constants/propTypes';

const Content = ({ players, messages, newMessage }) => {
  return (
    <ContentWrapper>
      <ul>
        {messages.ids.map(id => {
          const { player, text } = messages.byId[id];
          return (
            <li key={id}>
              <b>{players.byId[player].name}</b>: {text}
            </li>
          );
        })}
        {newMessage}
      </ul>
    </ContentWrapper>
  );
};
Content.propTypes = {
  players: playersType.isRequired,
  messages: messagesType.isRequired,
  newMessage: node.isRequired
};
const ContentWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  flex: 65;
  margin: 10px 0 0 20px;
  overflow-y: scroll;
  ${'' /* background-color: blue; */}
  ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
    word-wrap: break-word;
  }
`;

export default Content;
