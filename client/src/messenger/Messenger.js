import React from 'react';
import styled from 'styled-components';
import { arrayOf, element } from 'prop-types';

const Messenger = ({ children }) => (
  <MessengerWrapper>
    {children}
  </MessengerWrapper>
);
Messenger.propTypes = {
  children: arrayOf(element.isRequired).isRequired
};
const MessengerWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-end;
  width: 100%;
  flex: 1;
  background-color: white;
  border-radius: 10px;
`;

export default Messenger;
