import React, { Fragment } from 'react';
import styled from 'styled-components';
import { arrayOf, func, string } from 'prop-types';

import Row from './Row';
import Cell from './Cell';
import Grid from './Grid';
import Avatar from '../avatar/Avatar';
import { playersType } from '../constants/propTypes';

const Board = ({ players, moves, handleMove }) => {
  const size = Math.sqrt(moves.length);
  const rows = Array(size).fill(null);
  const cols = Array(size).fill(null);
  return (
    <BoardWrapper>
      {rows.map((_, row) => (
        <Fragment key={row}>
          {row > 0 && <Grid first last/>}
          <Row>
            {cols.map((_, col) => {
              const position = size * row + col;
              const player = players.byId[moves[position]];
              return (
                <Fragment key={col}>
                  {col > 0 &&
                    <Grid
                      first={row === 0}
                      last={row === rows.length - 1}/>}
                  <Cell data-position={position} onClick={handleMove}>
                    {player && <Avatar data-position={position} name={player.avatar}/>}
                  </Cell>
                </Fragment>
              );
            })}
          </Row>
        </Fragment>
      ))}
    </BoardWrapper>
  );
};
Board.propTypes = {
  players: playersType.isRequired,
  moves: arrayOf(string).isRequired,
  handleMove: func.isRequired
};
const BoardWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: stretch;
  width: 60vw;
  height: 60vw;
  max-width: 250px;
  max-height: 250px;
`;

export default Board;
