import React from 'react';
import styled from 'styled-components';
import { element } from 'prop-types';

const Cell = props => (
  <CellWrapper {...props}>
    {props.children}
  </CellWrapper>
);
Cell.propTypes = {
  children: element
};
const CellWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  flex: 1;
`;

export default Cell;
