import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Board from './Board';
import Row from './Row';
import Cell from './Cell';
import Grid from './Grid';
import Avatar from '../avatar/Avatar';

Enzyme.configure({adapter: new Adapter()});

let props, wrapper;

describe('Board', () => {
  beforeEach(() => {
    props = {
      players: {
        byId: {},
        ids: []
      },
      moves: Array(9).fill(null),
      handleMove: jest.fn(),
      dispatch: jest.fn()
    };
    wrapper = shallow(<Board {...props}/>);
  });
  test('should render component', () => {
    // 3*3 board should consist of 3 rows, 9 cells, and 8 grids
    expect(wrapper.find(Row).length).toBe(3);
    expect(wrapper.find(Cell).length).toBe(9);
    expect(wrapper.find(Grid).length).toBe(8);
    // no moves should mean no avatars on board
    expect(wrapper.find(Avatar).exists()).toBe(false);
  });
  test('should render moves on board', () => {
    wrapper.setProps({
      players: {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CIRCLE',
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      },
      moves: [
        'player_1', null, null,
        null, 'player_1', null,
        'player_2', null, null
      ]
    });
    expect(wrapper.find(Row).length).toBe(3);
    expect(wrapper.find(Cell).length).toBe(9);
    expect(wrapper.find(Grid).length).toBe(8);
    expect(wrapper.find(Avatar).length).toBe(3);
    expect(wrapper.find(
      'Cell[data-position=0] Avatar[name="CROSS"]'
    ).exists()).toBe(true);
    expect(wrapper.find(
      'Cell[data-position=4] Avatar[name="CROSS"]'
    ).exists()).toBe(true);
    expect(wrapper.find(
      'Cell[data-position=6] Avatar[name="CIRCLE"]'
    ).exists()).toBe(true);
  });
});
