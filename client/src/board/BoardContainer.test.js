import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { BoardContainer, mapDispatchToProps } from './BoardContainer';
import Board from './Board';
import { MAX_MOVES } from '../constants/gameConstants';

jest.mock('../actions/actionHelpers');
jest.mock('../actions/actions');
import * as actionHelpers from '../actions/actionHelpers';
import * as actions from '../actions/actions';
import { EVENT_MOVE } from '../constants/actionTypes';

Enzyme.configure({adapter: new Adapter()});

let props, wrapper;

describe('BoardContainer', () => {
  beforeEach(() => {
    props = {
      players: {
        byId: {},
        ids: []
      },
      moves: Array(MAX_MOVES).fill(null),
      handleMove: jest.fn(),
      dispatch: jest.fn()
    };
    wrapper = shallow(<BoardContainer {...props}/>);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('should render component', () => {
    expect(wrapper.find(Board).exists()).toBe(true);
    // componentDidMount
    expect(props.dispatch).toHaveBeenCalledTimes(1);
    expect(actionHelpers.addSocketListener).toHaveBeenCalledTimes(1);
    expect(actionHelpers.addSocketListener).toHaveBeenCalledWith(
      EVENT_MOVE,
      actions.performMove
    );
    props.dispatch.mockClear();
    // componentWillUnmount
    wrapper.unmount();
    expect(props.dispatch).toHaveBeenCalledTimes(1);
    expect(actionHelpers.removeSocketListener).toHaveBeenCalledTimes(1);
    expect(actionHelpers.removeSocketListener).toHaveBeenCalledWith(EVENT_MOVE);
  });
  test('handleMove should be called with the board position', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).handleMove(
      {target: {dataset: {position: 5}}}
    );
    expect(dispatch).toHaveBeenCalledTimes(1);
  });
});
