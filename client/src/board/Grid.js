import React from 'react';
import styled from 'styled-components';
import { bool } from 'prop-types';

const Grid = props => (
  <GridWrapper {...props}>
    {props.children}
  </GridWrapper>
);
Grid.propTypes = {
  first: bool,
  last: bool
};
const GridWrapper = styled.div`
  border: 10px solid white;
  border-top-left-radius: ${props => props.first ? '10px' : ''};
  border-top-right-radius: ${props => props.first ? '10px' : ''};
  border-bottom-left-radius: ${props => props.last ? '10px' : ''};
  border-bottom-right-radius: ${props => props.last ? '10px' : ''};
`;

export default Grid;
