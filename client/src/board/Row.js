import React from 'react';
import styled from 'styled-components';
import { element, arrayOf } from 'prop-types';

const Row = props => (
  <RowWrapper {...props}>
    {props.children}
  </RowWrapper>
);
Row.propTypes = {
  children: arrayOf(element.isRequired).isRequired
};
const RowWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: stretch;
  flex: 1;

`;

export default Row;
