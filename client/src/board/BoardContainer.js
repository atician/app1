import React, { Component } from 'react';
import { connect } from 'react-redux';

import Board from './Board';
import { performMove } from '../actions/actions';
import {
  handleMove,
  addSocketListener,
  removeSocketListener
} from '../actions/actionHelpers';
import { EVENT_MOVE } from '../constants/actionTypes';

export class BoardContainer extends Component {
  componentDidMount() {
    this.props.dispatch(addSocketListener(EVENT_MOVE, performMove));
  }
  componentWillUnmount() {
    this.props.dispatch(removeSocketListener(EVENT_MOVE));
  }
  render() {
    return (
      <Board {...this.props}/>
    );
  }
}
BoardContainer.propTypes = {
  ...Board.propTypes
};
const mapStateToProps = ({ players, moves }) => ({
  players,
  moves
});
export const mapDispatchToProps = dispatch => ({
  dispatch,
  handleMove: (e) => dispatch(handleMove(Number(e.target.dataset.position)))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoardContainer);
