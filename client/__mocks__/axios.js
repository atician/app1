const axios = jest.genMockFromModule('axios');

axios._initPost = res => {
  axios.post.res = res;
};
axios.post = jest.fn(() => {
  return new Promise((resolve, reject) => {
    if ('data' in axios.post.res) {
      resolve(axios.post.res);
    } else {
      reject(axios.post.res);
    }
  });
});

axios._initPatch = res => {
  axios.patch.res = res;
};
axios.patch = jest.fn(() => {
  return new Promise((resolve, reject) => {
    if ('data' in axios.patch.res) {
      resolve(axios.patch.res);
    } else {
      reject(axios.patch.res);
    }
  });
});

axios._initGet = res => {
  axios.get.res = res;
};
axios.get = jest.fn(() => {
  return new Promise((resolve, reject) => {
    if ('data' in axios.get.res) {
      resolve(axios.get.res);
    } else {
      reject(axios.get.res);
    }
  });
});
export default axios;
