exports.MAX_PLAYERS = 2;
exports.MAX_MOVES = 9;
exports.MAX_NAME_LENGTH = 10;
exports.MAX_MESSAGE_LENGTH = 256;
