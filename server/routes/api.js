const express = require('express');
const router = express.Router();

const roomController = require('../controllers/roomController');
const playerController = require('../controllers/playerController');

// POST request to create player and join room
router.post('/player/:playerId', playerController.createPlayer);
// PATCH request to update player's avatar
router.patch('/room/:roomId/player/:playerId/avatar', playerController.updatePlayer);
// GET request for players in room
router.get('/room/:roomId/player/:playerId', roomController.getPlayers);
// POST request to perform move
router.post('/room/:roomId/move', roomController.createMove);
// POST request to reset game
router.post('/room/:roomId/reset', roomController.resetGame);
// POST request to start game
router.post('/room/:roomId/start', roomController.startGame);
// POST request to send message
router.post('/room/:roomId/message', roomController.createMessage);
// GET app status
// router.get('/status', (req, res) => {
//   const locals = req.app.locals;
//   res.json({
//     sockets: Object.keys(locals.sockets),
//     players: Object.keys(locals.players),
//     rooms: locals.rooms
//   });
// })
module.exports = router;
