const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const apiRouter = require('./routes/api');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

app.use(express.static(path.join(__dirname, '../client/build')));
app.use(bodyParser.json());

app.use('/api', apiRouter);

// all sockets, indexed by socket.id (player id)
app.locals.sockets = {};
// all players, indexed by player id
// players = {
//   'player_1': {
//     id: 'player_1',
//     name: 'Joe',
//     avatar: 'CROSS',
//     score: 0,
//     room: 'room_1'
//   }
// }
app.locals.players = {};
// all game rooms, indexed by room id
// rooms = {
//   'room_1': {
//     id: 'room_1',
//     players: ['player_1'],
//     moves: Array(9).fill(null),
//     currPlayer: 'player_1',
//     winner: 'player_1',
//     mode: 'MULTI_PLAYER',
//     isOpen: true
//   }
// }
app.locals.rooms = {};

io.on('connection', socket => {
  console.log(`a user connected ${socket.id}`);
  app.locals.sockets[socket.id] = socket;
  socket.on('disconnect', (reason) => {
    console.log(`user disconnected ${reason}`);
    const locals = app.locals;
    // remove player's socket
    delete locals.sockets[socket.id];
    if (!locals.players[socket.id]) {
      return;
    }
    // remove player from room
    const roomId = locals.players[socket.id].room;
    if (roomId) {
      const room = locals.rooms[roomId];
      const playerIndex = room.players.indexOf(socket.id);
      room.players.splice(playerIndex, 1);
      // remove room if no more players
      if (!room.players.length) {
        delete locals.rooms[roomId];
      }
    }
    // remove player
    delete locals.players[socket.id];
  });
});

const port = process.env.PORT || 8000;
server.listen(port, () => console.log(`Listening on port ${port}`));
