const roomController = require('./roomController');
const httpMocks = require('node-mocks-http');
const EVENTS = require('../constants/eventConstants');
const { MAX_MOVES, MAX_PLAYERS, MAX_MESSAGE_LENGTH } = require('../constants/gameConstants');

jest.mock('../utils/broadcast');
jest.mock('../utils/getRoomPlayers');
jest.mock('../utils/getChosenAvatars');
jest.mock('../utils/getWinner');
const broadcast = require('../utils/broadcast');
const getRoomPlayers = require('../utils/getRoomPlayers');
const getChosenAvatars = require('../utils/getChosenAvatars');
const getWinner = require('../utils/getWinner');

describe('roomController', () => {
  let res, app;
  beforeEach(() => {
    app = {
      locals: {
        sockets: {},
        players: {},
        rooms: {}
      }
    };
    res = httpMocks.createResponse();
    // add response mocks
    res.json = jest.fn();
    res.end = jest.fn();
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('getPlayers should return players in first room', () => {
    let req = httpMocks.createRequest({
      params: {
        roomId: 'room_1',
        playerId: 'player_1'
      }
    });
    // add app locals to req
    req.app = app;
    app.locals.players = {
      'player_1': {
        id: 'player_1',
        name: 'Joe',
        avatar: null,
        score: 0,
        room: 'room_1'
      },
      'player_2': {
        id: 'player_2',
        name: 'Jane',
        avatar: null,
        score: 0,
        room: 'room_1'
      },
      'player_3': {
        id: 'player_3',
        name: 'John',
        avatar: null,
        score: 0,
        room: 'room_2'
      }
    };
    app.locals.rooms = {
      'room_1': {
        id: 'room_1',
        players: ['player_1', 'player_2'],
        moves: null,
        currPlayer: null,
        winner: null,
        mode: 'MULTI_PLAYER',
        isOpen: false
      },
      'room_2': {
        id: 'room_2',
        players: ['player_3'],
        moves: null,
        currPlayer: null,
        winner: null,
        mode: 'MULTI_PLAYER',
        isOpen: true
      }
    };
    const roomPlayers = {
      byId: {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: null,
          score: 0
        }
      },
      ids: ['player_1', 'player_2']
    };
    getRoomPlayers.mockReturnValue(roomPlayers);
    roomController.getPlayers(req, res);
    expect(getRoomPlayers).toHaveBeenCalledTimes(1);
    expect(getRoomPlayers).toHaveBeenCalledWith(
      'player_1',
      {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: null,
          score: 0,
          room: 'room_1'
        },
        'player_3': {
          id: 'player_3',
          name: 'John',
          avatar: null,
          score: 0,
          room: 'room_2'
        }
      },
      {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        },
        'room_2': {
          id: 'room_2',
          players: ['player_3'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: true
        }
      }
    );
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({players: roomPlayers});
  });
  describe('createMove', () => {
    test('should not perform move because it is not player\'s turn', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1',
          position: 4
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: Array(MAX_MOVES).fill(null),
          currPlayer: 'player_2',
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      roomController.createMove(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should not perform move because position is occupied', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1',
          position: 4
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: [
            null, null, null,
            null, 'player_2', null,
            null, null, null
          ],
          currPlayer: 'player_1',
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      roomController.createMove(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should not perform move because there is already a winner', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1',
          position: 4
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: Array(MAX_MOVES).fill(null),
          currPlayer: 'player_1',
          winner: 'player_2',
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      roomController.createMove(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should perform move', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1',
          position: 4
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: Array(MAX_MOVES).fill(null),
          currPlayer: 'player_1',
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      // set mock winner
      getWinner.mockReturnValue('player_1');
      roomController.createMove(req, res);
      expect(res.end).not.toHaveBeenCalled();
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {},
            'player_2': {}
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: 'CROSS',
              score: 0,
              room: 'room_1'
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: 'CIRCLE',
              score: 0,
              room: 'room_1'
            }
          },
          rooms: {
            'room_1': {
              id: 'room_1',
              players: ['player_1', 'player_2'],
              moves: [
                null, null, null,
                null, 'player_1', null,
                null, null, null
              ],
              currPlayer: 'player_2',
              winner: 'player_1',
              mode: 'MULTI_PLAYER',
              isOpen: false
            }
          }
        }
      });
      expect(getWinner).toHaveBeenCalledTimes(1);
      expect(getWinner).toHaveBeenCalledWith([
        null, null, null,
        null, 'player_1', null,
        null, null, null
      ]);
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        {},
        'room_1',
        EVENTS.EVENT_MOVE,
        {position: 4}
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({position: 4});
    });
  });
  describe('resetGame', () => {
    test('should not reset because player is not winner', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: Array(MAX_MOVES).fill(null),
          currPlayer: 'player_1',
          winner: 'player_2',
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      roomController.resetGame(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should not reset because player is not first on leader board', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_2'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: Array(MAX_MOVES).fill('player_1'),
          currPlayer: 'player_1',
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      roomController.resetGame(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should reset because player is winner', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_2'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: Array(MAX_MOVES).fill('player_1'),
          currPlayer: 'player_1',
          winner: 'player_2',
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      const roomPlayers = {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CIRCLE',
            score: 1
          }
        },
        ids: ['player_2', 'player_1']
      };
      getRoomPlayers.mockReturnValue(roomPlayers);
      roomController.resetGame(req, res);
      expect(res.end).not.toHaveBeenCalled();
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {},
            'player_2': {}
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: 'CROSS',
              score: 0,
              room: 'room_1'
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: 'CIRCLE',
              // increment score for winner
              score: 1,
              room: 'room_1'
            }
          },
          rooms: {
            'room_1': {
              id: 'room_1',
              // reorder players on leader board
              players: ['player_2', 'player_1'],
              // reset moves
              moves: Array(MAX_MOVES).fill(null),
              // reset next player to first player on leader board
              currPlayer: 'player_2',
              // reset winner
              winner: null,
              mode: 'MULTI_PLAYER',
              isOpen: false
            }
          }
        }
      });
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_2',
        {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0,
            room: 'room_1'
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CIRCLE',
            score: 1,
            room: 'room_1'
          }
        },
        {
          'room_1': {
            id: 'room_1',
            players: ['player_2', 'player_1'],
            moves: Array(MAX_MOVES).fill(null),
            currPlayer: 'player_2',
            winner: null,
            mode: 'MULTI_PLAYER',
            isOpen: false
          }
        }
      );
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        {},
        'room_1',
        EVENTS.EVENT_RESET,
        {players: roomPlayers}
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({players: roomPlayers});
    });
    test('should reset without score change because game is a tie', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_2'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_2', 'player_1'],
          moves: Array(MAX_MOVES).fill('player_1'),
          currPlayer: 'player_1',
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      const roomPlayers = {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CIRCLE',
            score: 0
          }
        },
        ids: ['player_2', 'player_1']
      };
      getRoomPlayers.mockReturnValue(roomPlayers);
      roomController.resetGame(req, res);
      expect(res.end).not.toHaveBeenCalled();
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {},
            'player_2': {}
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: 'CROSS',
              score: 0,
              room: 'room_1'
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: 'CIRCLE',
              score: 0,
              room: 'room_1'
            }
          },
          rooms: {
            'room_1': {
              id: 'room_1',
              players: ['player_2', 'player_1'],
              // reset moves
              moves: Array(MAX_MOVES).fill(null),
              // reset next player to first player on leader board
              currPlayer: 'player_2',
              // reset winner
              winner: null,
              mode: 'MULTI_PLAYER',
              isOpen: false
            }
          }
        }
      });
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_2',
        {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0,
            room: 'room_1'
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CIRCLE',
            score: 0,
            room: 'room_1'
          }
        },
        {
          'room_1': {
            id: 'room_1',
            players: ['player_2', 'player_1'],
            moves: Array(MAX_MOVES).fill(null),
            currPlayer: 'player_2',
            winner: null,
            mode: 'MULTI_PLAYER',
            isOpen: false
          }
        }
      );
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        {},
        'room_1',
        EVENTS.EVENT_RESET,
        {players: roomPlayers}
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({players: roomPlayers});
    });
  });
  describe('startGame', () => {
    test('should not start game because player is not first on leader board', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_2'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      const roomPlayers = {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CIRCLE',
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      };
      getRoomPlayers.mockReturnValue(roomPlayers);
      roomController.startGame(req, res);
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_2',
        {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0,
            room: 'room_1'
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CIRCLE',
            score: 0,
            room: 'room_1'
          }
        },
        {
          'room_1': {
            id: 'room_1',
            players: ['player_1', 'player_2'],
            moves: null,
            currPlayer: null,
            winner: null,
            mode: 'MULTI_PLAYER',
            isOpen: false
          }
        }
      );
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should not start game because not all players have chosen an avatar', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: null,
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      const roomPlayers = {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      };
      getRoomPlayers.mockReturnValue(roomPlayers);
      getChosenAvatars.mockReturnValue(['CROSS']);
      roomController.startGame(req, res);
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_1',
        {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0,
            room: 'room_1'
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 0,
            room: 'room_1'
          }
        },
        {
          'room_1': {
            id: 'room_1',
            players: ['player_1', 'player_2'],
            moves: null,
            currPlayer: null,
            winner: null,
            mode: 'MULTI_PLAYER',
            isOpen: false
          }
        }
      );
      expect(getChosenAvatars).toHaveBeenCalledTimes(1);
      expect(getChosenAvatars).toHaveBeenCalledWith(roomPlayers);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should start game', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: 'CIRCLE',
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      const roomPlayers = {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: 'CIRCLE',
            score: 0
          }
        },
        ids: ['player_1', 'player_2']
      };
      getRoomPlayers.mockReturnValue(roomPlayers);
      getChosenAvatars.mockReturnValue(['CROSS', 'CIRCLE']);
      roomController.startGame(req, res);
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_1',
        app.locals.players,
        app.locals.rooms
      );
      expect(getChosenAvatars).toHaveBeenCalledTimes(1);
      expect(getChosenAvatars).toHaveBeenCalledWith(roomPlayers);
      expect(res.end).not.toHaveBeenCalled();
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {},
            'player_2': {}
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: 'CROSS',
              score: 0,
              room: 'room_1'
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: 'CIRCLE',
              score: 0,
              room: 'room_1'
            }
          },
          rooms: {
            'room_1': {
              id: 'room_1',
              players: ['player_1', 'player_2'],
              // expect moves to be initialized
              moves: Array(MAX_MOVES).fill(null),
              // expect next player to be first player on leader board
              currPlayer: 'player_1',
              // expect winner to be initialized
              winner: null,
              mode: 'MULTI_PLAYER',
              isOpen: false
            }
          }
        }
      });
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        {},
        'room_1',
        EVENTS.EVENT_START,
        {maxMoves: MAX_MOVES}
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({maxMoves: MAX_MOVES});
    });
  });
  describe('createMessage', () => {
    // mock message id
    const messageId = 1534418787646;
    test('should not relay message because there is no text', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      roomController.createMessage(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should not relay message because text is too long', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1',
          text: 'a'.repeat(MAX_MESSAGE_LENGTH + 1)
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      roomController.createMessage(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should relay message', () => {
      let req = httpMocks.createRequest({
        params: {
          roomId: 'room_1'
        },
        body: {
          player: 'player_1',
          text: 'Hey'
        }
      });
      // add app locals to req
      req.app = app;
      app.locals.sockets = {
        'player_1': {},
        'player_2': {}
      };
      // spy on message id creation
      const dateSpy = jest.spyOn(Date, 'now').mockImplementation(() => messageId);
      roomController.createMessage(req, res);
      expect(res.end).not.toHaveBeenCalled();
      expect(dateSpy).toHaveBeenCalledTimes(1);
      expect(dateSpy).toHaveReturnedWith(messageId);
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        {},
        'room_1',
        EVENTS.EVENT_MESSAGE,
        {
          message: {
            id: messageId,
            player: 'player_1',
            text: 'Hey'
          }
        }
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({
        message: {
          id: messageId,
          player: 'player_1',
          text: 'Hey'
        }
      });
    });
  });
});
