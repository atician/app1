const playerController = require('./playerController');
const httpMocks = require('node-mocks-http');
const EVENTS = require('../constants/eventConstants');

jest.mock('../utils/broadcast');
jest.mock('../utils/getRoomPlayers');
jest.mock('../utils/getChosenAvatars');
const broadcast = require('../utils/broadcast');
const getRoomPlayers = require('../utils/getRoomPlayers');
const getChosenAvatars = require('../utils/getChosenAvatars');

describe('playerController', () => {
  // mock room id
  const roomId = 1534418787646;
  let res, app;
  beforeEach(() => {
    app = {
      locals: {
        sockets: {},
        players: {},
        rooms: {}
      }
    };
    res = httpMocks.createResponse();
    // add response mocks
    res.json = jest.fn();
    res.end = jest.fn();
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  describe('createPlayer', () => {
    // spy on room id creation
    const dateSpy = jest.spyOn(Date, 'now').mockImplementation(() => roomId);
    test('should not create player because name is empty', () => {
      // mock request
      const req = httpMocks.createRequest({
        params: {
          playerId: 'player_1'
        },
        body: {
          name: '',
          mode: 'MULTI_PLAYER'
        }
      });
      // add app locals to req
      req.app = app;
      playerController.createPlayer(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should not create player because name is too long', () => {
      // mock request
      const req = httpMocks.createRequest({
        params: {
          playerId: 'player_1'
        },
        body: {
          name: 'JoeHasALongName',
          mode: 'MULTI_PLAYER'
        }
      });
      // add app locals to req
      req.app = app;
      playerController.createPlayer(req, res);
      expect(res.end).toHaveBeenCalledTimes(1);
    });
    test('should create first player and add to new room', () => {
      // mock request
      const req = httpMocks.createRequest({
        params: {
          playerId: 'player_1'
        },
        body: {
          name: 'Joe',
          mode: 'MULTI_PLAYER'
        }
      });
      // add app locals to req
      req.app = app;
      // mock sockets
      const sockets = {
        'player_1': {
          join: jest.fn()
        }
      };
      app.locals.sockets = sockets;
      // mock return values
      getRoomPlayers.mockReturnValue(null);
      playerController.createPlayer(req, res);
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {
              join: sockets['player_1'].join
            }
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: null,
              score: 0,
              room: roomId
            }
          },
          rooms: {
            [roomId]: {
              id: roomId,
              players: ['player_1'],
              moves: null,
              currPlayer: null,
              winner: null,
              mode: 'MULTI_PLAYER',
              isOpen: true
            }
          }
        }
      });
      expect(dateSpy).toHaveBeenCalledTimes(1);
      expect(dateSpy).toHaveReturnedWith(roomId);
      expect(sockets['player_1'].join).toHaveBeenCalledTimes(1);
      expect(sockets['player_1'].join).toHaveBeenCalledWith(roomId)
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_1',
        {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: null,
            score: 0,
            room: roomId
          }
        },
        {
          [roomId]: {
            id: roomId,
            players: ['player_1'],
            moves: null,
            currPlayer: null,
            winner: null,
            mode: 'MULTI_PLAYER',
            isOpen: true
          }
        }
      );
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        sockets['player_1'],
        roomId,
        EVENTS.EVENT_PLAYER,
        {players: null}
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({room: roomId});
    });
    test('should create second player and add to existing room', () => {
      // mock request
      const req = httpMocks.createRequest({
        params: {
          playerId: 'player_2'
        },
        body: {
          name: 'Jane',
          mode: 'MULTI_PLAYER'
        }
      });
      // add app locals to req
      req.app = app;
      // mock sockets
      const sockets = {
        'player_1': {
          join: jest.fn()
        },
        'player_2': {
          join: jest.fn()
        }
      };
      app.locals.sockets = sockets;
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0,
          room: roomId
        }
      };
      app.locals.rooms = {
        [roomId]: {
          id: roomId,
          players: ['player_1'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: true
        }
      };
      // mock return values
      getRoomPlayers.mockReturnValue(null);
      playerController.createPlayer(req, res);
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {
              join: sockets['player_1'].join
            },
            'player_2': {
              join: sockets['player_2'].join
            }
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: null,
              score: 0,
              room: roomId
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: null,
              score: 0,
              room: roomId
            }
          },
          rooms: {
            [roomId]: {
              id: roomId,
              players: ['player_1', 'player_2'],
              moves: null,
              currPlayer: null,
              winner: null,
              mode: 'MULTI_PLAYER',
              // room should be closed once player limit is reached
              isOpen: false
            }
          }
        }
      });
      // no new room should have been created
      expect(dateSpy).not.toHaveBeenCalled();
      // player_1 should already be joined to socket
      expect(sockets['player_1'].join).not.toHaveBeenCalled();
      expect(sockets['player_2'].join).toHaveBeenCalledTimes(1);
      expect(sockets['player_2'].join).toHaveBeenCalledWith(roomId);
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_2',
        {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: null,
            score: 0,
            room: roomId
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 0,
            room: roomId
          }
        },
        {
          [roomId]: {
            id: roomId,
            players: ['player_1', 'player_2'],
            moves: null,
            currPlayer: null,
            winner: null,
            mode: 'MULTI_PLAYER',
            isOpen: false
          }
        }
      );
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        sockets['player_2'],
        roomId,
        EVENTS.EVENT_PLAYER,
        {players: null}
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({room: roomId});
    });
    test('should create third player and add to new room', () => {
      // mock request
      const req = httpMocks.createRequest({
        params: {
          playerId: 'player_3'
        },
        body: {
          name: 'John',
          mode: 'MULTI_PLAYER'
        }
      });
      // add app locals to req
      req.app = app;
      // mock sockets
      const sockets = {
        'player_1': {
          join: jest.fn()
        },
        'player_2': {
          join: jest.fn()
        },
        'player_3': {
          join: jest.fn()
        }
      };
      app.locals.sockets = sockets;
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0,
          room: 'room_1'
        },
        'player_2': {
          id: 'player_2',
          name: 'Jane',
          avatar: null,
          score: 0,
          room: 'room_1'
        }
      };
      app.locals.rooms = {
        'room_1': {
          id: 'room_1',
          players: ['player_1', 'player_2'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: false
        }
      };
      // mock return values
      getRoomPlayers.mockReturnValue(null);
      playerController.createPlayer(req, res);
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {
              join: sockets['player_1'].join
            },
            'player_2': {
              join: sockets['player_2'].join
            },
            'player_3': {
              join: sockets['player_3'].join
            }
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: null,
              score: 0,
              room: 'room_1'
            },
            'player_2': {
              id: 'player_2',
              name: 'Jane',
              avatar: null,
              score: 0,
              room: 'room_1'
            },
            'player_3': {
              id: 'player_3',
              name: 'John',
              avatar: null,
              score: 0,
              room: roomId
            }
          },
          rooms: {
            'room_1': {
              id: 'room_1',
              players: ['player_1', 'player_2'],
              moves: null,
              currPlayer: null,
              winner: null,
              mode: 'MULTI_PLAYER',
              isOpen: false
            },
            [roomId]: {
              id: roomId,
              players: ['player_3'],
              moves: null,
              currPlayer: null,
              winner: null,
              mode: 'MULTI_PLAYER',
              isOpen: true
            }
          }
        }
      });
      // new room should have been created
      expect(dateSpy).toHaveBeenCalledTimes(1);
      expect(dateSpy).toHaveReturnedWith(roomId);
      // player_1 and player_2 should already be joined to socket
      expect(sockets['player_1'].join).not.toHaveBeenCalled();
      expect(sockets['player_2'].join).not.toHaveBeenCalled();
      expect(sockets['player_3'].join).toHaveBeenCalledTimes(1);
      expect(sockets['player_3'].join).toHaveBeenCalledWith(roomId);
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_3',
        {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: null,
            score: 0,
            room: 'room_1'
          },
          'player_2': {
            id: 'player_2',
            name: 'Jane',
            avatar: null,
            score: 0,
            room: 'room_1'
          },
          'player_3': {
            id: 'player_3',
            name: 'John',
            avatar: null,
            score: 0,
            room: roomId
          }
        },
        {
          'room_1': {
            id: 'room_1',
            players: ['player_1', 'player_2'],
            moves: null,
            currPlayer: null,
            winner: null,
            mode: 'MULTI_PLAYER',
            isOpen: false
          },
          [roomId]: {
            id: roomId,
            players: ['player_3'],
            moves: null,
            currPlayer: null,
            winner: null,
            mode: 'MULTI_PLAYER',
            isOpen: true
          }
        }
      );
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        sockets['player_3'],
        roomId,
        EVENTS.EVENT_PLAYER,
        {players: null}
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({room: roomId});
    });
  });
  describe('updatePlayer', () => {
    test('should update player\'s avatar', () => {
      // mock request
      const req = httpMocks.createRequest({
        params: {
          playerId: 'player_1',
          roomId
        },
        body: {
          avatar: 'CROSS'
        }
      });
      // add app locals to req
      req.app = app;
      // mock sockets
      const sockets = {
        'player_1': {
          join: jest.fn()
        }
      };
      app.locals.sockets = sockets;
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: null,
          score: 0,
          room: roomId
        }
      };
      app.locals.rooms = {
        [roomId]: {
          id: roomId,
          players: ['player_1'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: true
        }
      };
      // mock chosen avatars
      const roomPlayers = {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: null,
            score: 0
          }
        },
        ids: ['player_1']
      };
      getRoomPlayers.mockReturnValue(roomPlayers);
      getChosenAvatars.mockReturnValue([]);
      playerController.updatePlayer(req, res);
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {
              join: sockets['player_1'].join
            }
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: 'CROSS',
              score: 0,
              room: roomId
            }
          },
          rooms: {
            [roomId]: {
              id: roomId,
              players: ['player_1'],
              moves: null,
              currPlayer: null,
              winner: null,
              mode: 'MULTI_PLAYER',
              isOpen: true
            }
          }
        }
      });
      // expect mock functions to have been called with correct args
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_1',
        app.locals.players,
        app.locals.rooms
      );
      // check mock value is returned
      expect(getRoomPlayers).toHaveReturnedWith(roomPlayers);
      expect(getChosenAvatars).toHaveBeenCalledTimes(1);
      expect(getChosenAvatars).toHaveBeenCalledWith(roomPlayers);
      // check mock value is returned
      expect(getChosenAvatars).toHaveReturnedWith([]);
      expect(broadcast).toHaveBeenCalledTimes(1);
      expect(broadcast).toHaveBeenCalledWith(
        sockets['player_1'],
        roomId,
        EVENTS.EVENT_AVATAR,
        {
          player: 'player_1',
          avatar: 'CROSS'
        }
      );
      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({
        player: 'player_1',
        avatar: 'CROSS'
      });
    });
    test('should not update player\'s avatar because it is selected', () => {
      // mock request
      const req = httpMocks.createRequest({
        params: {
          playerId: 'player_1',
          roomId
        },
        body: {
          avatar: 'CROSS'
        }
      });
      // add app locals to req
      req.app = app;
      // mock sockets
      const sockets = {
        'player_1': {
          join: jest.fn()
        }
      };
      app.locals.sockets = sockets;
      app.locals.players = {
        'player_1': {
          id: 'player_1',
          name: 'Joe',
          avatar: 'CROSS',
          score: 0,
          room: roomId
        }
      };
      app.locals.rooms = {
        [roomId]: {
          id: roomId,
          players: ['player_1'],
          moves: null,
          currPlayer: null,
          winner: null,
          mode: 'MULTI_PLAYER',
          isOpen: true
        }
      };
      // mock chosen avatars
      const roomPlayers = {
        byId: {
          'player_1': {
            id: 'player_1',
            name: 'Joe',
            avatar: 'CROSS',
            score: 0
          }
        },
        ids: ['player_1']
      };
      getRoomPlayers.mockReturnValue(roomPlayers);
      getChosenAvatars.mockReturnValue(['CROSS']);
      playerController.updatePlayer(req, res);
      // no change in state because avatar is already selected
      expect(app).toEqual({
        locals: {
          sockets: {
            'player_1': {
              join: sockets['player_1'].join
            }
          },
          players: {
            'player_1': {
              id: 'player_1',
              name: 'Joe',
              avatar: 'CROSS',
              score: 0,
              room: roomId
            }
          },
          rooms: {
            [roomId]: {
              id: roomId,
              players: ['player_1'],
              moves: null,
              currPlayer: null,
              winner: null,
              mode: 'MULTI_PLAYER',
              isOpen: true
            }
          }
        }
      });
      // expect mock functions to have been called with correct args
      expect(getRoomPlayers).toHaveBeenCalledTimes(1);
      expect(getRoomPlayers).toHaveBeenCalledWith(
        'player_1',
        app.locals.players,
        app.locals.rooms
      );
      // check mock value is returned
      expect(getRoomPlayers).toHaveReturnedWith(roomPlayers);
      expect(getChosenAvatars).toHaveBeenCalledTimes(1);
      expect(getChosenAvatars).toHaveBeenCalledWith(roomPlayers);
      // check mock value is returned
      expect(getChosenAvatars).toHaveReturnedWith(['CROSS']);
      expect(res.end).toHaveBeenCalledTimes(1);
      expect(broadcast).not.toHaveBeenCalled();
      expect(res.json).not.toHaveBeenCalled();
    });
  });
});
