const getRoomPlayers = require('../utils/getRoomPlayers');
const getChosenAvatars = require('../utils/getChosenAvatars');
const getWinner = require('../utils/getWinner');
const broadcast = require('../utils/broadcast');
const EVENTS = require('../constants/eventConstants');
const { MAX_MOVES, MAX_PLAYERS, MAX_MESSAGE_LENGTH } = require('../constants/gameConstants');

// get all players in room
exports.getPlayers = (req, res) => {
  const locals = req.app.locals;
  const { roomId, playerId } = req.params;
  res.json({players: getRoomPlayers(playerId, locals.players, locals.rooms)});
};
// perform player's move and set the next player and winner, if any
exports.createMove = (req, res) => {
  const locals = req.app.locals;
  const { roomId } = req.params;
  const { player:playerId, position } = req.body;
  const room = locals.rooms[roomId];
  // check if move is valid
  if (playerId !== room.currPlayer
      || room.moves[position]
      || room.winner) {
    res.end();
    return;
  }
  // move is valid, perform move and set next player and winner, if any
  room.moves[position] = playerId;
  room.winner = getWinner(room.moves);
  const currPlayerIndex = room.players.indexOf(playerId);
  room.currPlayer = room.players[(currPlayerIndex + 1) % room.players.length];
  // send position of move to all players in room
  broadcast(locals.sockets[playerId], roomId, EVENTS.EVENT_MOVE, {position});
  res.json({position});
};
// reset game state if there is a winner or a tie
exports.resetGame = (req, res) => {
  const locals = req.app.locals;
  const { roomId } = req.params;
  const { player:playerId } = req.body;
  const room = locals.rooms[roomId];
  const isTie = !room.winner && !room.moves.filter(move => !move).length;
  // only allow winner or first player to reset game
  if ((room.winner && room.winner !== playerId)
      || (isTie && room.players[0] !== playerId)) {
    res.end();
    return;
  }
  // increment winner's score
  if (room.winner === playerId) {
    ++locals.players[playerId].score;
    room.players.sort((a, b) => (
      locals.players[b].score - locals.players[a].score
    ));
  }
  // reset moves, next player and winner
  room.moves.fill(null);
  room.currPlayer = room.players[0];
  room.winner = null;
  // send updated players to players in room
  const data = {
    players: getRoomPlayers(playerId, locals.players, locals.rooms)
  };
  broadcast(locals.sockets[playerId], roomId, EVENTS.EVENT_RESET, data);
  res.json(data);
};
// start game if each player has an avatar
exports.startGame = (req, res) => {
  const locals = req.app.locals;
  const { roomId } = req.params;
  const { player:playerId } = req.body;
  const roomPlayers = getRoomPlayers(playerId, locals.players, locals.rooms);
  // game can only start if each player has an avatar, and
  // only first player in room can start
  if (roomPlayers.ids[0] !== playerId
      || getChosenAvatars(roomPlayers).length < MAX_PLAYERS) {
    res.end();
    return;
  }
  // initialize moves, next player, and close room to start game
  const room = locals.rooms[roomId];
  room.moves = Array(MAX_MOVES).fill(null);
  room.currPlayer = playerId;
  room.isOpen = false;
  // send max number of moves in game to all players in room
  const data = {
    maxMoves: MAX_MOVES
  };
  broadcast(locals.sockets[playerId], roomId, EVENTS.EVENT_START, data);
  res.json(data);
};
// relay message to other players in room
// messages are only stored on the clients
exports.createMessage = (req, res) => {
  const locals = req.app.locals;
  const { roomId } = req.params;
  const { player:playerId, text } = req.body;
  if (!text || text.length > MAX_MESSAGE_LENGTH ) {
    res.end();
  }
  const id = Date.now();
  const message = {
    id,
    player: playerId,
    text
  };
  broadcast(locals.sockets[playerId], roomId, EVENTS.EVENT_MESSAGE, {message});
  res.json({message});
};
