const getRoomPlayers = require('../utils/getRoomPlayers');
const getChosenAvatars = require('../utils/getChosenAvatars');
const broadcast = require('../utils/broadcast');
const EVENTS = require('../constants/eventConstants');
const { MAX_PLAYERS, MAX_NAME_LENGTH } = require('../constants/gameConstants');

// add player and place them into a room
exports.createPlayer = (req, res) => {
  const locals = req.app.locals;
  const { playerId } = req.params;
  const { name, mode } = req.body;
  if (!name || name.length > MAX_NAME_LENGTH) {
    res.end();
    return;
  }
  let room = null;
  // if there is an open room, add the player to that
  Object.keys(locals.rooms).forEach(roomId => {
    room = locals.rooms[roomId];
    if (room.isOpen && room.mode === mode) {
      room.players.push(playerId);
      if (room.players.length === MAX_PLAYERS) {
        room.isOpen = false;
      }
    } else {
      room = null;
    }
  });
  // there is no open room, create one and add the player to that
  if (!room) {
    const roomId = Date.now();
    room = {
      id: roomId,
      players: [playerId],
      moves: null,
      currPlayer: null,
      winner: null,
      mode,
      isOpen: true
    };
    locals.rooms[roomId] = room;
  }
  // add the player to the array of players
  const player = {
    id: playerId,
    name,
    avatar: null,
    score: 0,
    room: room.id
  };
  locals.players[playerId] = player;
  // add player to socket room
  locals.sockets[playerId].join(room.id);
  // broadcast new player to game room
  broadcast(
    locals.sockets[playerId],
    room.id,
    EVENTS.EVENT_PLAYER,
    {players: getRoomPlayers(playerId, locals.players, locals.rooms)}
  );
  res.json({room: room.id});
};
// update player's avatar
exports.updatePlayer = (req, res) => {
  const locals = req.app.locals;
  const { roomId, playerId } = req.params;
  const { avatar } = req.body;
  // get chosen avatars by players in room
  const roomPlayers = getRoomPlayers(playerId, locals.players, locals.rooms);
  const chosenAvatars = getChosenAvatars(roomPlayers);
  if (chosenAvatars.includes(avatar)) {
    res.end();
    return;
  }
  // update player's avatar
  locals.players[playerId].avatar = avatar;
  const data = {
    player: playerId,
    avatar
  };
  broadcast(locals.sockets[playerId], roomId, EVENTS.EVENT_AVATAR, data);
  res.json(data);
};
