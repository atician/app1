const getChosenAvatars = players => (
  players.ids.map(id => players.byId[id].avatar).filter(avatar => avatar)
);
module.exports = getChosenAvatars;
