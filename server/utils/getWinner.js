function getWinner(moves) {
  const winningCells = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < winningCells.length; ++i) {
    const [a, b, c] = winningCells[i];
    if (typeof moves[a] === 'string' && moves[a] === moves[b] && moves[b] === moves[c] && moves[a] === moves[c]) {
      return moves[a];
    }
  }
  return null;
}

module.exports = getWinner;
