const getRoomPlayers = (playerId, players, rooms) => {
  let roomPlayers = {
    byId: {},
    ids: []
  };
  const room = rooms[players[playerId].room];
  room.players.forEach(id => {
    const { room, ...player } = players[id];
    roomPlayers.byId[id] = {...player};
  });
  roomPlayers.ids = [...room.players];
  return roomPlayers;
};
module.exports = getRoomPlayers;
