const broadcast = (socket, id, event, data) => {
  socket.broadcast.to(id).emit(event, data);
};
module.exports = broadcast;
